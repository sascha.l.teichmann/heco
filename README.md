# Heco - A time taking tool for the terminal

This is a small time tracking tool for the terminal which stores
the data in an [SQLite3](https://sqlite.org) database.

![heco in action](./images/heco.png)

It is mainly a re-write of the old [getan](https://wald.intevation.org/projects/getan/)
tool done to get rid of some rusty old code and add some new features.

## Build

Use need a working [Go](https://golang.org) develepment setup.
You can get the needed files from [here](https://golang.org/dl).
Tested successfully with Go 1.24.0.

```
$ go install gitlab.com/sascha.l.teichmann/heco/cmd/heco@latest
```

Put the resulting `heco` binary into your `PATH`.

# Usage

By default heco will use a database file in `~/.heco/time.db`.
If it does not exists it will create one.
To see all the command line options use `heco --help`.

After starting `heco` you see two windows. On the left is the window
with the **projects** on the right the one for the **entries**.  
A **project** in terms of heco is a account to book time to.
An **entry** is a unit of tracked time.

When the program is freshly started the project window is focused.
In an empty database there is no project so this window is empty, too.
To create a new project press the `Insert` key on your keyboard.
(For remapping see [Configuration](./docs/configuration.md)). The window on the right
will change to give that new project a keyboard shortcut and
a description. The shortcut must be unique and is used to start
the time taking on this particular project.

Once you have setup your project you are ready to time-taking.
Simply start typing the shortcut of the project you wish to
take time for. As soon as the shortcut is distinct the time taking
will begin. Or your can simply press `enter` on a marked
project.

In time taking mode the bottom line of the screen will show you
how much time is already gone since you have started the current
time taking. You can pause the time taking by pressing the `space` key.
Another press of `space` will resume the time taking. If you
want to add oder substract time to press `+` or `-`. You
will be prompted for a respective amount of time to be added
or take off your already recorded time duration.

If you want to end the time-taking press the `enter` key. You
will be prompted than to give a short description to this measurement.
After giving this the entry will be ready and added to the entries
in the entry window.

Heco sums up time for you. There are a daily, a weekly (selected by default),
a monthly, a yearly and a total mode. You can cycle through this
modes by pressing `F1`.

Once you have created a project you can press the `tab` key to switch to the
entries window. Another `tab` press brings you back to the project window.
In the entries you can edit there description  by pressing `e` on the marked
entry. `cursor up` and `cursor down` will move the selection.
Pressing `d` will delete an entry (after confirming). `m` will
let you move an entry to another project. `l` will allow
you to alter the duration of the entry, `a` will alter the start time.
To join entries press `j`. This will update the oldest entry to
be as long as all the selected entries together after entering
a common description text. The other entries will be removed.
If you want to split an entry press `s`. After entering a duration
a second entry with this duration will be created. The first
one will be shortend by this duration. and both will be continuous
placed one after each other. Pressing `i` will allow you
to create entries without prior time-taking. This is useful
if you want to add them if you were e.g. unable to take
them directly in the first place.

All operations could be canceled by pressing the `escape` key.
If the `escape` key without an operation in progress heco will
ask you to quit the program. This will also work when the project window
is active.

If you are in the entries window your can mark more than one of them
by hitting the `space` key and move to others and do that again. This
is useful when you want to delete or move more than one at a time.

## Configuration

See [Configuration](./docs/configuration.md).

## License

This is Free Software covered by the terms of the Apache License, Version 2.0.  
See [LICENSE](LICENSES/apache-2.0.txt) for details.  
Copyright 2020 by the [CONTRIBUTORS](CONTRIBUTORS).
