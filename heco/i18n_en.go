// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

var langEN = LangLookup{
	"insert.project":        "Projekt anlegen",
	"ok":                    "Okay",
	"cancel":                "Cancel",
	"edit.project":          "Edit project",
	"project.key":           "Key",
	"project.description":   "Description",
	"choose.project":        "Choose a project.",
	"choose.entries":        "Choose at least one entry.",
	"selected.project":      "Selected:",
	"really.quit":           "Really quit? (y/n)",
	"enter.description":     "Enter a description:",
	"enter.time.add":        "Enter time to add [min[]:",
	"enter.time.sub":        "Enter time to subtract [min[]:",
	"space.continue":        "Press 'Space' to continue.",
	"running.on":            "Running ( %s ) on '%s'.",
	"break":                 "Break ( %s ) %s",
	"really.delete.entry":   "Really delete this/these entry/entries? (y/n)",
	"edit.entry.text":       "Edit entry text:",
	"edit.entry.length":     "Edit entry duration:",
	"edit.entry.start":      "Edit entry start:",
	"new.entry":             "New entry:",
	"new.entry.start":       "Start time:",
	"new.entry.length":      "Duration:",
	"new.entry.description": "Description:",
	"join.entry":            "Common text:",
	"split.entry":           "duration to split:",
	"move.into.which":       "destination project:",
	"are.you.sure":          "Are you sure? (y/n)",
	"total":                 "total",
	"year":                  "year",
	"month":                 "month",
	"week":                  "week",
	"last_week":             "last week",
	"day":                   "day",
	"all.projects":          "All projects: %s %s",
	"heco":                  "~: heco :~",
	"version":               "version",
	"projects.title":        "projects (%s)",
	"entries":               "entries",
}
