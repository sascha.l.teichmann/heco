package heco

import (
	"context"
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"sync"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib" // register the sql driver.
)

const (
	loadProjectEntriesPGSQL = `
SELECT
    id,
    start_time,
    stop_time,
    description
FROM
    entries
WHERE
    project_id = $1
ORDER BY
    start_time
DESC`

	loadRecoverPGSQL = `
SELECT
    id,
    project_id,
    start_time,
    stop_time,
    description
FROM
   recover
WHERE
    id = 1`

	insertRecoverPGSQL = `
INSERT INTO recover(id, project_id, start_time, stop_time, description)
VALUES(1, $1, $2, $3, $4)
ON CONFLICT (id)
DO UPDATE SET project_id = $1, start_time = $2, stop_time = $3, description = $4
`
)

var repqm = sync.OnceValue(func() *regexp.Regexp {
	return regexp.MustCompile(`(\?)`)
})

func replaceQuestionMarks(s string) string {
	index := 1
	return repqm().ReplaceAllStringFunc(s, func(string) (r string) {
		r = "$" + strconv.Itoa(index)
		index++
		return
	})
}

type PGBackend struct {
	CommonBackend
}

func NewPGBackend(_ context.Context, database string) (*PGBackend, error) {
	db, err := sql.Open("pgx", database)
	if err != nil {
		return nil, fmt.Errorf("opening postgresql backend failed: %w", err)
	}
	return &PGBackend{
		CommonBackend{
			db: db,
		},
	}, nil
}

func (b *PGBackend) LoadRecover() error {
	return b.CommonBackend.LoadRecover(
		loadRecoverPGSQL,
		replaceQuestionMarks(insertProjectEntrySQL))
}

func (b *PGBackend) LoadEntries(projectID int64) (Entries, error) {
	return b.CommonBackend.LoadEntries(loadProjectEntriesPGSQL, projectID)
}

func (b *PGBackend) insertEntry(projectID int64, start, stop time.Time, desc string) (int64, error) {
	return b.CommonBackend.insertEntry(
		replaceQuestionMarks(insertProjectEntrySQL),
		projectID, start, stop, desc)
}

func (b *PGBackend) insertRecover(projectID int64, start, stop time.Time, desc string) error {
	return b.CommonBackend.insertRecover(
		insertRecoverPGSQL,
		projectID, start, stop, desc)
}

func (b *PGBackend) updateProject(p *Project) error {
	return b.CommonBackend.updateProject(replaceQuestionMarks(updateProjectSQL), p)
}

func (b *PGBackend) insertProject(key, desc string) (int64, error) {
	return b.CommonBackend.insertProject(replaceQuestionMarks(insertProjectSQL), key, desc)
}

func (b *PGBackend) splitEntry(projectID int64, entry *Entry, duration time.Duration) (*Entry, error) {
	return b.CommonBackend.splitEntry(
		replaceQuestionMarks(insertProjectEntrySQL),
		replaceQuestionMarks(updateEntryEndSQL),
		projectID, entry, duration)
}

func (b *PGBackend) joinEntries(desc string, entries Entries) error {
	return b.CommonBackend.joinEntries(
		replaceQuestionMarks(joinSQL),
		replaceQuestionMarks(deleteProjectEntrySQL),
		desc, entries)
}

func (b *PGBackend) moveEntries(entries Entries, newID int64) error {
	return b.CommonBackend.moveEntries(replaceQuestionMarks(moveEntrySQL), entries, newID)
}

func (b *PGBackend) updateEntry(entry *Entry) error {
	return b.CommonBackend.updateEntry(replaceQuestionMarks(updateEntrySQL), entry)
}

func (b *PGBackend) deleteEntries(entries Entries) error {
	return b.CommonBackend.deleteEntries(replaceQuestionMarks(deleteProjectEntrySQL), entries)
}
