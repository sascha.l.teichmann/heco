// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"errors"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"
)

var (
	// Be a bit more tolerant than needed to stay editable.
	timeEditRe  = regexp.MustCompile(`^(\d*)(?::(\d*))?(?::(\d*))?$`)
	timeFinalRe = regexp.MustCompile(`^(\d+)(?::(\d+))?(?::(\d+))?$`)
	dateEditRe  = regexp.MustCompile(`^(\d*)(-|)(\d*)(-|)(\d*)\s*(\d*)(:|)(\d*)(:|)(\d*)$`)
	dateFinalRe = regexp.MustCompile(`^(\d+)-(\d{1,2})-(\d{1,2})\s+(\d{1,2}):(\d{1,2}):(\d{1,2})$`)
)

const (
	datetimeFormat = "2006-01-02"
	dateFormat     = "02.01.2006"
)

func formatDatetime(t time.Time) string {
	return t.Format(datetimeFormat)
}

func DateFormat(t time.Time) string {
	return t.Format(dateFormat)
}

func ShortTime(d time.Duration) string {
	minutes := int(math.Round(d.Minutes()))
	return fmt.Sprintf("%d:%02d", minutes/60, minutes%60)
}

func HumanTimeFormatted(d time.Duration, zero string) string {
	if d == 0 {
		return zero
	}
	seconds := int(d.Seconds())
	s := seconds % 60
	seconds /= 60
	m := seconds % 60
	seconds /= 60
	return fmt.Sprintf("%02d:%02d:%02d", seconds, m, s)
}

func HumanTime(d time.Duration) string {
	return HumanTimeFormatted(d, "--:--:--")
}

func removeSpace(s string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, s)
}

func acceptPositiveInteger(s string, ch rune) bool {
	if ch == '-' {
		return false
	}
	_, err := strconv.Atoi(s)
	return err == nil
}

func acceptTimeLength(s string, _ rune) bool {
	return timeEditRe.MatchString(s)
}

func parseTimeLength(s string) (time.Duration, error) {
	m := timeFinalRe.FindStringSubmatch(s)

	var hours, mins, secs int
	switch {
	case m == nil:
		return 0, errors.New("does not match")
	case m[2] == "":
		mins, _ = strconv.Atoi(m[1])
	case m[3] == "":
		mins, _ = strconv.Atoi(m[1])
		secs, _ = strconv.Atoi(m[2])
	default:
		hours, _ = strconv.Atoi(m[1])
		mins, _ = strconv.Atoi(m[2])
		secs, _ = strconv.Atoi(m[3])
	}

	return time.Duration(hours)*time.Hour +
		time.Duration(mins)*time.Minute +
		time.Duration(secs)*time.Second, nil
}

func acceptDate(s string, _ rune) bool {
	return dateEditRe.MatchString(s)
}

func parseDate(s string) (time.Time, error) {
	m := dateFinalRe.FindStringSubmatch(s)
	if m == nil {
		return time.Time{}, errors.New("does not match")
	}
	var (
		year, _  = strconv.Atoi(m[1])
		month, _ = strconv.Atoi(m[2])
		day, _   = strconv.Atoi(m[3])
		hours, _ = strconv.Atoi(m[4])
		mins, _  = strconv.Atoi(m[5])
		secs, _  = strconv.Atoi(m[6])
	)
	return time.Date(
		year, time.Month(month), day,
		hours, mins, secs,
		0, time.Local), nil
}
