// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type ProjectList struct {
	view *View

	table *tview.Table

	selector    *tview.Pages
	information *tview.TextView
	question    *tview.InputField
	chooser     *tview.InputField

	sort ProjectSortMode

	cursor bool
}

type EntryList struct {
	view *View

	list *tview.List

	selector    *tview.Pages
	information *tview.TextView
	question    *tview.InputField

	selected map[int]struct{}
	isSpace  bool
}

type Footer struct {
	view *View

	selector    *tview.Pages
	information *tview.TextView
	question    *tview.InputField
}

type View struct {
	ctrl *Controller

	leftSelector *tview.Pages
	entryMeta    *EntryMeta
	projects     *ProjectList

	rightSelector *tview.Pages
	projectMeta   *ProjectMeta
	entries       *EntryList

	footer *Footer
}

type ProjectMeta struct {
	view *View
	form *tview.Form
}

type EntryMeta struct {
	view *View
	form *tview.Form
}

func BuildView(app *tview.Application, ctrl *Controller) error {

	ctrl.view = &View{ctrl: ctrl}

	header := ctrl.view.buildHeader()

	projects, err := ctrl.view.buildProjects()
	if err != nil {
		return err
	}

	em := ctrl.view.buildEntryMeta()

	ctrl.view.leftSelector = tview.NewPages().
		AddPage("entry-meta", em, true, false).
		AddPage("projects", projects, true, true)

	entries, err := ctrl.view.buildEntries()
	if err != nil {
		return err
	}

	pm := ctrl.view.buildProjectMeta()

	ctrl.view.rightSelector = tview.NewPages().
		AddPage("project-meta", pm, true, false).
		AddPage("entries", entries, true, true)

	body := tview.NewFlex().
		SetDirection(tview.FlexColumn).
		AddItem(ctrl.view.leftSelector, 0, 1, true).
		AddItem(ctrl.view.rightSelector, 0, 1, true)

	body.
		SetBackgroundColor(ctrl.config.theme.Body.Backgound)

	footer := ctrl.view.buildFooter()

	main := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(header, 1, 0, false).
		AddItem(body, 0, 1, false).
		AddItem(footer, 1, 0, false)

	app.SetRoot(main, true).SetFocus(ctrl.view.projects.table)
	return nil
}

func (v *View) buildEntryMeta() tview.Primitive {

	em := &EntryMeta{
		view: v,
		form: tview.NewForm(),
	}

	theme := v.ctrl.config.theme

	em.form.SetBorder(true)
	em.form.
		SetFieldTextColor(theme.Body.Foreground).
		SetBackgroundColor(theme.Body.Backgound)

	v.entryMeta = em

	return em.form
}

func (v *View) buildProjectMeta() tview.Primitive {

	pm := &ProjectMeta{
		view: v,
		form: tview.NewForm(),
	}

	theme := v.ctrl.config.theme

	pm.form.SetBorder(true)
	pm.form.
		SetFieldTextColor(theme.Body.Foreground).
		SetBackgroundColor(theme.Body.Backgound)

	v.projectMeta = pm

	return pm.form
}

func (v *View) buildProjects() (tview.Primitive, error) {

	v.projects = &ProjectList{
		view: v,
		sort: v.ctrl.config.Mode.Sort,
	}

	if err := v.projects.buildTable(); err != nil {
		return nil, err
	}

	v.projects.buildStatus()

	flex := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(v.projects.table, 0, 1, true).
		AddItem(v.projects.selector, 1, 0, false)

	return flex, nil
}

func (pl *ProjectList) switchTimeMode() error {

	pl.view.ctrl.cycleMode()

	if err := pl.buildTable(); err != nil {
		return err
	}
	if err := pl.view.entries.fillEntries(); err != nil {
		return err
	}
	pl.allProjectsTime()
	return nil
}

func (pl *ProjectList) switchToProject() error {
	id := pl.view.ctrl.project.ID
	row := -1
	for i, n := 0, pl.table.GetRowCount(); i < n; i++ {
		if tid, ok := pl.table.GetCell(i, 0).Reference.(int64); ok && tid == id {
			row = i
			break
		}
	}
	if row == -1 {
		return nil
	}
	pl.table.Select(row, 0)
	if err := pl.view.entries.fillEntries(); err != nil {
		return err
	}
	pl.allProjectsTime()
	return nil
}

func (pl *ProjectList) switchProjectOrder() error {
	if pl.sort++; pl.sort > treeSortMode {
		pl.sort = idSortMode
	}

	if err := pl.buildTable(); err != nil {
		return err
	}
	return nil
}

func (pl *ProjectList) buildStatus() {

	theme := pl.view.ctrl.config.theme
	pl.chooser = tview.NewInputField().
		SetLabel(Translate("selected.project") + " ").
		SetAutocompleteFunc(pl.chooserAutocomplete).
		SetAcceptanceFunc(pl.chooserAccept)

	pl.chooser.SetInputCapture(pl.chooserCapture)

	pl.chooser.SetLabelColor(theme.Running.Foreground).
		SetBackgroundColor(theme.Running.Backgound)

	pl.information = tview.NewTextView()

	pl.question = tview.NewInputField()

	pl.selector = tview.NewPages().
		AddPage("question", pl.question, true, false).
		AddPage("chooser", pl.chooser, true, false).
		AddPage("information", pl.information, true, true)

	pl.allProjectsTime()
}

func (pl *ProjectList) allProjectsTime() {
	ctrl := pl.view.ctrl
	mode := ctrl.getMode()
	duration := ctrl.projects.Duration(mode)
	text := fmt.Sprintf(
		Translate("all.projects"),
		Translate(mode.String()),
		HumanTime(duration))
	pl.info(ctrl.config.theme.ProjectFooter, text)
}

func (pl *ProjectList) ask(te ThemeEntry, label, text string) {
	pl.question.
		SetLabelColor(te.Foreground)
	pl.question.
		SetBackgroundColor(te.Backgound)
	pl.question.
		SetFieldBackgroundColor(te.Backgound)

	pl.question.SetLabel(label)
	pl.question.SetText(text)
	pl.selector.SwitchToPage("question")
	pl.view.ctrl.app.SetFocus(pl.question)
}

func (pl *ProjectList) info(te ThemeEntry, text string) {
	pl.information.
		SetTextColor(te.Foreground).
		SetBackgroundColor(te.Backgound)
	pl.information.
		SetText(text)
	pl.selector.SwitchToPage("information")
}

func (pl *ProjectList) chooserCapture(event *tcell.EventKey) *tcell.EventKey {
	pl.cursor = event.Key() == tcell.KeyDown || event.Key() == tcell.KeyUp
	return event
}

func (pl *ProjectList) onSelected(action func(*Project), cancelled func()) {
	pl.chooser.SetChangedFunc(func(text string) {
		if text == "" || pl.cursor {
			return
		}
		var count int
		var match *Project
		for _, p := range pl.view.ctrl.projects {
			if strings.HasPrefix(p.Key, text) {
				match = p
				if count++; count > 1 {
					break
				}
			}
		}
		if count == 1 {
			pl.resetChooser()
			action(match)
		}
	})
	pl.chooser.SetDoneFunc(func(key tcell.Key) {
		text := pl.chooser.GetText()
		pl.resetChooser()

		if key != tcell.KeyEnter {
			pl.view.ctrl.state = ProjectState
			pl.view.ctrl.app.SetFocus(pl.view.projects.table)
			if cancelled != nil {
				cancelled()
			}
			return
		}

		ps := pl.view.ctrl.projects.sortBy(pl.view.projects.sort)

		var match *Project
		for _, p := range ps {
			if p.Key == text {
				match = p
				break
			}
			if match == nil && strings.HasPrefix(p.Key, text) {
				match = p
			}
		}
		if match != nil {
			Debugf("action\n")
			action(match)
		} else if cancelled != nil {
			cancelled()
		}
	})
}

func (pl *ProjectList) resetChooser() {
	pl.chooser.SetText("")
	pl.selector.SwitchToPage("information")
}

func (pl *ProjectList) chooserAccept(text string, _ rune) bool {
	for _, p := range pl.view.ctrl.projects {
		if strings.HasPrefix(p.Key, text) {
			return true
		}
	}
	return false
}

func (pl *ProjectList) chooserAutocomplete(text string) []string {
	if text == "" || pl.view == nil {
		return nil
	}
	var matches []string
	ps := pl.view.ctrl.projects.sortBy(pl.view.projects.sort)
	for _, p := range ps {
		if strings.HasPrefix(p.Key, text) {
			matches = append(matches, p.Key)
		}
	}
	return matches
}

func (pl *ProjectList) changedEntry(row, _ int) {
	if row < 0 || len(pl.view.ctrl.projects) < row {
		return
	}
	ps := pl.view.ctrl.projects.sortBy(pl.view.projects.sort)
	pl.view.ctrl.project = ps[row]

	if err := pl.view.entries.fillEntries(); err != nil {
		Errorf("Filling entries window failed: %v.\n", err)
	}
}

func (pl *ProjectList) selectProject() {
	id := pl.view.ctrl.project.ID
	for i, n := 0, pl.table.GetRowCount(); i < n; i++ {
		if pl.table.GetCell(i, 0).Reference.(int64) == id {
			pl.table.Select(i, 0)
			break
		}
	}
}

func (pl *ProjectList) updateProjectTime(project *Project) {
	// Update the time of the project.
	for i, n := 0, pl.table.GetRowCount(); i < n; i++ {
		if pl.table.GetCell(i, 0).Reference.(int64) == project.ID {
			var duration string
			mode := pl.view.ctrl.getMode()
			if d := project.Duration(mode); d != 0 {
				duration = HumanTime(d)
			}
			pl.table.GetCell(i, 2).SetText(duration)
			break
		}
	}
}

func (pl *ProjectList) buildTable() error {

	theme := pl.view.ctrl.config.theme

	if pl.table == nil {
		pl.table = tview.NewTable().
			SetSelectionChangedFunc(pl.changedEntry).
			SetSelectable(true, false).
			SetSelectedStyle(
				tcell.StyleDefault.
					Foreground(theme.FocusedEntry.Foreground).
					Background(theme.FocusedEntry.Backgound))
		pl.table.
			SetBackgroundColor(theme.Entry.Backgound)
	}

	var keyStyle = tcell.StyleDefault.
		Foreground(theme.ProjectKey.Foreground).
		Background(theme.ProjectKey.Backgound).
		Bold(true)

	ps := pl.view.ctrl.projects.sortBy(pl.view.projects.sort)

	newPos := -1

	mode := pl.view.ctrl.getMode()

	for i, p := range ps {
		if p == pl.view.ctrl.project {
			newPos = i
		}
		if _, err := p.GetEntries(pl.view.ctrl.backend); err != nil {
			return err
		}

		var indent string
		if pl.view.projects.sort == treeSortMode && i > 0 {
			a, _ := utf8.DecodeRuneInString(p.Key)
			b, _ := utf8.DecodeRuneInString(ps[i-1].Key)
			if a == b {
				indent = "  "
			}
		}

		pl.table.SetCell(i, 0,
			tview.NewTableCell(indent+p.Key).
				SetReference(p.ID).
				SetStyle(keyStyle).
				SetAlign(tview.AlignLeft))

		pl.table.SetCell(i, 1,
			tview.NewTableCell(p.Desc).
				SetAlign(tview.AlignLeft).
				SetExpansion(1))

		var duration string
		if d := p.Duration(mode); d != 0 {
			duration = HumanTime(d)
		}

		pl.table.SetCell(i, 2,
			tview.NewTableCell(duration).
				SetAlign(tview.AlignRight))
	}

	if newPos != -1 {
		pl.table.Select(newPos, 0)
	}

	pl.table.SetBorder(true)
	pl.table.SetTitle(pl.title())
	return nil
}

func (pl *ProjectList) title() string {
	mode := Translate(pl.view.ctrl.getMode().String())
	return fmt.Sprintf(" "+Translate("projects.title")+" ", mode)
}

func (v *View) buildHeader() tview.Primitive {

	theme := v.ctrl.config.theme

	prg := tview.NewTextView()
	prg.SetTextColor(theme.Header.Foreground).
		SetBackgroundColor(theme.Header.Backgound)
	prg.SetText(Translate("heco"))

	ver := tview.NewTextView()
	ver.SetTextAlign(tview.AlignRight).
		SetTextColor(theme.Header.Foreground).
		SetBackgroundColor(theme.Header.Backgound)
	ver.SetText(Translate("version") + " " + Version)

	return tview.NewFlex().
		SetDirection(tview.FlexColumn).
		AddItem(prg, 0, 1, false).
		AddItem(ver, 0, 1, false)
}

func (v *View) buildFooter() tview.Primitive {

	v.footer = &Footer{
		view:        v,
		selector:    tview.NewPages(),
		information: tview.NewTextView(),
		question:    tview.NewInputField(),
	}

	theme := v.ctrl.config.theme

	v.footer.information.
		SetTextColor(theme.Info.Foreground).
		SetBackgroundColor(theme.Info.Backgound)

	v.footer.information.
		SetText(Translate("choose.project"))

	v.footer.selector.
		AddPage("information", v.footer.information, true, true).
		AddPage("question", v.footer.question, true, false)

	return v.footer.selector
}

func (em *EntryMeta) editEntry(
	title string,
	entry *Entry,
	fn func(*Entry, bool),
) {
	em.form.Clear(true)
	em.form.SetTitle(" " + title + " ")

	n := *entry

	em.form.AddInputField(
		Translate("new.entry.start"),
		entry.Start.Format("2006-01-02 15:04:05"),
		0,
		acceptDate,
		func(text string) {
			if t, err := parseDate(text); err == nil {
				diff := n.End.Sub(n.Start)
				n.Start = t
				n.End = n.Start.Add(diff)
			}
		})

	duration := entry.End.Sub(entry.Start)
	em.form.AddInputField(
		Translate("new.entry.length"),
		HumanTimeFormatted(duration, "00:00:00"),
		0,
		acceptTimeLength,
		func(text string) {
			if d, err := parseTimeLength(text); err == nil {
				n.End = n.Start.Add(d)
			}
		})
	em.form.AddInputField(
		Translate("new.entry.description"),
		entry.Desc,
		0,
		nil,
		func(text string) { n.Desc = text })

	done := func(ok bool) {
		em.form.Clear(true)

		if ok && (n.Start.Equal(entry.Start) &&
			n.End.Equal(entry.End) &&
			n.Desc == entry.Desc) {
			ok = false
		}

		fn(&n, ok)
	}

	em.form.SetCancelFunc(func() { done(false) })

	em.form.AddButton(Translate("ok"), func() { done(true) })
	em.form.AddButton(Translate("cancel"), func() { done(false) })

	em.view.leftSelector.SwitchToPage("entry-meta")
	em.view.ctrl.app.SetFocus(em.form)
}

func (pm *ProjectMeta) editProject(
	title string,
	oldKey, oldDesc string,
	fn func(string, string, bool)) error {

	keys, err := pm.view.ctrl.backend.AllProjectKeys()
	if err != nil {
		return err
	}

	hasKey := func(s string) bool {
		for _, key := range keys {
			if key == s {
				return true
			}
		}
		return false
	}

	pm.form.Clear(true)
	pm.form.SetTitle(" " + title + " ")

	key, desc := oldKey, oldDesc

	pm.form.AddInputField(
		Translate("project.key"),
		oldKey,
		0,
		func(text string, _ rune) bool { return text == oldKey || !hasKey(text) },
		func(text string) { key = text })

	pm.form.AddInputField(
		Translate("project.description"),
		oldDesc,
		0,
		nil,
		func(text string) { desc = text })

	done := func(ok bool) {
		pm.form.Clear(true)
		if key != oldKey && hasKey(key) {
			ok = false
		}
		fn(key, desc, ok)
	}

	pm.form.SetCancelFunc(func() { done(false) })

	pm.form.AddButton(Translate("ok"), func() { done(true) })
	pm.form.AddButton(Translate("cancel"), func() { done(false) })

	pm.view.rightSelector.SwitchToPage("project-meta")
	pm.view.ctrl.app.SetFocus(pm.form)

	return nil
}

func (f *Footer) ask(te ThemeEntry, label, text string) {
	f.question.
		SetLabelColor(te.Foreground).
		SetFieldBackgroundColor(te.Backgound).
		SetBackgroundColor(te.Backgound)

	f.question.SetLabel(label)
	f.question.SetText(text)
	f.selector.SwitchToPage("question")
	f.view.ctrl.app.SetFocus(f.question)
}

func (f *Footer) info(te ThemeEntry, text string) {
	f.information.
		SetTextColor(te.Foreground).
		SetBackgroundColor(te.Backgound)
	f.information.SetText(text)
	f.selector.SwitchToPage("information")
}

func (el *EntryList) setCurrentEntry(entry *Entry, filter func(*Entry) bool) bool {
	var idx int
	var success bool
	el.view.ctrl.project.Entries.walk(
		filter,
		func(e *Entry) {
			if e == entry {
				el.list.SetCurrentItem(idx)
				success = true
			}
			idx++
		})
	return success
}

func (el *EntryList) fillEntries() error {
	if el == nil {
		return nil
	}

	ctrl := el.view.ctrl

	if ctrl.project == nil {
		return nil
	}

	entries, err := ctrl.project.GetEntries(ctrl.backend)
	if err != nil {
		return err
	}

	el.list.Clear()

	entries.walk(
		el.view.ctrl.getModeFilter(),
		func(e *Entry) {
			text := e.SelectedText(false)
			el.list.AddItem(text, "", 0, el.toggleEntrySelection)
		})

	if len(el.selected) > 0 {
		el.selected = make(map[int]struct{})
	}

	return nil
}

func (el *EntryList) toggleEntrySelection() {
	if !el.isSpace {
		return
	}
	ctrl := el.view.ctrl
	entries, err := ctrl.project.GetEntries(ctrl.backend)
	if err != nil {
		Errorf("unable to load entries: %v\n", err)
		return
	}

	i := el.list.GetCurrentItem()

	entry := entries.nTh(el.view.ctrl.getModeFilter(), i)
	if entry == nil {
		Warnf("could not find entry #%d\n", i)
		return
	}

	_, selected := el.selected[i]
	if selected {
		delete(el.selected, i)
	} else {
		el.selected[i] = struct{}{}
	}
	selected = !selected

	el.list.SetItemText(i, entry.SelectedText(selected), "")
}

func (el *EntryList) inputCapture(event *tcell.EventKey) *tcell.EventKey {
	el.isSpace = event.Rune() == ' '
	return event
}

func (el *EntryList) info(te ThemeEntry, text string) {
	el.information.
		SetTextColor(te.Foreground).
		SetBackgroundColor(te.Backgound)
	el.information.SetText(text)
	el.selector.SwitchToPage("information")
}

func (el *EntryList) ask(te ThemeEntry, label, text string) {
	el.question.
		SetLabelColor(te.Foreground)
	el.question.
		SetBackgroundColor(te.Backgound)
	el.question.
		SetFieldBackgroundColor(te.Backgound)

	el.question.SetLabel(label)
	el.question.SetText(text)
	el.selector.SwitchToPage("question")
	el.view.ctrl.app.SetFocus(el.question)
}

func (v *View) buildEntries() (tview.Primitive, error) {

	theme := v.ctrl.config.theme

	list := tview.NewList().
		SetSelectedTextColor(theme.FocusedEntry.Foreground).
		SetSelectedBackgroundColor(theme.FocusedEntry.Backgound).
		SetHighlightFullLine(true).
		ShowSecondaryText(false)
	list.
		SetBorder(true).
		SetTitle(" " + Translate("entries") + " ").
		SetBackgroundColor(theme.Entry.Backgound)

	question := tview.NewInputField()

	v.entries = &EntryList{
		view:     v,
		list:     list,
		question: question,
		selected: make(map[int]struct{}),
	}

	if err := v.entries.fillEntries(); err != nil {
		return nil, err
	}
	list.SetInputCapture(v.entries.inputCapture)

	v.entries.information = tview.NewTextView()
	v.entries.information.SetTextColor(theme.EntryFooter.Foreground).
		SetBackgroundColor(theme.EntryFooter.Backgound)

	v.entries.selector = tview.NewPages().
		AddPage("information", v.entries.information, true, true).
		AddPage("question", v.entries.question, true, false)

	flex := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(list, 0, 1, true).
		AddItem(v.entries.selector, 1, 0, false)

	return flex, nil
}
