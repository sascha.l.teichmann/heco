// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

var langDE = LangLookup{
	"insert.project":        "Projekt anlegen",
	"ok":                    "Übernehmen",
	"cancel":                "Abbrechen",
	"edit.project":          "Projekt bearbeiten",
	"project.key":           "Kürzel",
	"project.description":   "Beschreibung",
	"choose.project":        "Wählen Sie ein Projekt aus.",
	"selected.project":      "Ausgewählt:",
	"choose.entries":        "Wählen Sie mindestens einen Eintrag aus.",
	"really.quit":           "Wirklich beenden? (j/n)",
	"enter.description":     "Geben Sie eine Beschreibung ein:",
	"enter.time.add":        "Zu addierende Zeit [min[]:",
	"enter.time.sub":        "Zu subtrahierende Zeit  [min[]:",
	"space.continue":        "Drücken Sie '%c' um fortzusetzen.",
	"running.on":            "Sie arbeiten ( %s ) an '%s'.",
	"break":                 "Pause ( %s ) %s",
	"really.delete.entry":   "Eintrag/Einträge wirklich löschen? (j/n)",
	"edit.entry.text":       "Neuer Text:",
	"edit.entry.length":     "Neue Dauer:",
	"edit.entry.start":      "Neuer Start:",
	"new.entry":             "Neuer Eintrag:",
	"new.entry.start":       "Startzeit:",
	"new.entry.length":      "Dauer:",
	"new.entry.description": "Beschreibung:",
	"join.entry":            "Gemeinsamer Text:",
	"split.entry":           "Abzuspaltende Dauer:",
	"move.into.which":       "Zielprojekt:",
	"are.you.sure":          "Sind Sie sicher? (j/n)",
	"total":                 "Gesamt",
	"year":                  "Jahr",
	"month":                 "Monat",
	"week":                  "Woche",
	"last_week":             "Letzte Woche",
	"day":                   "Tag",
	"all.projects":          "Alle Projekte: %s %s",
	"heco":                  "~: heco :~",
	"version":               "Version",
	"projects.title":        "Projekte (%s)",
	"entries":               "Einträge",
}
