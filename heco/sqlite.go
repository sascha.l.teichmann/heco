// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"context"
	"database/sql"
	"os"
	"path/filepath"
	"time"

	_ "github.com/mattn/go-sqlite3" // register the sql driver.
)

const DefaultDatabase = "time.db"

const (
	createProjectsSQL = `
CREATE TABLE IF NOT EXISTS projects (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    key         VARCHAR(16) NOT NULL CONSTRAINT unique_key UNIQUE,
    description VARCHAR(256),
    active      BOOLEAN DEFAULT 1
)`

	createEntriesSQL = `
CREATE TABLE IF NOT EXISTS entries (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    project_id  INTEGER REFERENCES projects(id),
    start_time  TIMESTAMP NOT NULL,
    stop_time   TIMESTAMP NOT NULL,
    description VARCHAR(256),

    CHECK (strftime('%s', start_time) <= strftime('%s', stop_time))
)`

	createRecoverSQL = `
CREATE TABLE IF NOT EXISTS recover(
    id          INTEGER PRIMARY KEY,
    project_id  INTEGER REFERENCES projects(id),
    start_time  TIMESTAMP NOT NULL,
    stop_time   TIMESTAMP NOT NULL,
    description VARCHAR(256),

    CHECK (strftime('%s', start_time) <= strftime('%s', stop_time))
)`

	createProjectEntriesIndexSQL = `
CREATE INDEX IF NOT EXISTS project_entries ON entries(project_id)`
)

var createTablesSQL = [...]string{
	createProjectsSQL,
	createEntriesSQL,
	createRecoverSQL,
	createProjectEntriesIndexSQL,
}

const (
	loadProjectEntriesSQL = `
SELECT
    id,
    start_time as "[timestamp]",
    stop_time  as "[timestamp]",
    description
FROM
    entries
WHERE
    project_id = ?
ORDER BY
    start_time
DESC`
)

const (
	insertProjectEntrySQL = `
INSERT INTO entries (project_id, start_time, stop_time, description)
VALUES(?, ?, ?, ?) RETURNING id`

	loadRecoverSQL = `
SELECT
    id,
    project_id,
    start_time as "[timestamp]",
    stop_time  as "[timestamp]",
    description
FROM
   recover
WHERE
    id = 1`

	insertRecoverSQL = `
INSERT OR REPLACE INTO recover(id, project_id, start_time, stop_time, description)
VALUES(1, ?, ?, ?, ?)`

	insertProjectSQL = `
INSERT INTO projects (key, description) VALUES (?, ?) RETURNING id`

	deleteProjectEntrySQL = `DELETE FROM entries WHERE id = ?`

	moveEntrySQL = `UPDATE entries SET project_id = ? WHERE id = ?`

	updateEntrySQL = `
UPDATE entries
    SET description = ?, start_time = ?, stop_time = ?
    WHERE id = ?`

	updateProjectSQL = `
UPDATE projects SET key = ?, description = ? WHERE id = ?`

	updateEntryEndSQL = `
UPDATE entries SET stop_time = ? WHERE id = ?`

	joinSQL = `
UPDATE entries SET stop_time = ?, description = ?
WHERE id = ?`
)

type SQLiteBackend struct {
	CommonBackend
}

func NewSQLiteBackend(ctx context.Context, database string) (*SQLiteBackend, error) {

	if len(database) == 0 {
		database = DefaultDatabase
	}

	db, err := ensureExists(ctx, database)
	if err != nil {
		return nil, err
	}

	b := SQLiteBackend{
		CommonBackend{
			db: db,
		},
	}

	return &b, nil
}

func ensureExists(ctx context.Context, database string) (*sql.DB, error) {

	dir := filepath.Dir(database)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err := os.MkdirAll(dir, 0700); err != nil {
			return nil, err
		}
	}

	Infof("Open database %s\n", database)

	db, err := sql.Open("sqlite3", "file:"+database+"?_fk=1")
	if err != nil {
		return nil, err
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		db.Close()
		return nil, err
	}
	defer tx.Rollback()

	for _, sql := range createTablesSQL {
		if _, err := tx.ExecContext(ctx, sql); err != nil {
			return nil, err
		}
	}

	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return db, nil
}

func (b *SQLiteBackend) LoadEntries(projectID int64) (Entries, error) {
	return b.CommonBackend.LoadEntries(loadProjectEntriesSQL, projectID)
}

func (b *SQLiteBackend) LoadRecover() error {
	return b.CommonBackend.LoadRecover(
		loadRecoverSQL,
		insertProjectEntrySQL)
}

func (b *SQLiteBackend) insertEntry(
	projectID int64,
	start, stop time.Time,
	desc string,
) (int64, error) {
	return b.CommonBackend.insertEntry(
		insertProjectEntrySQL,
		projectID, start, stop, desc)
}

func (b *SQLiteBackend) insertRecover(
	projectID int64,
	start, stop time.Time,
	desc string,
) error {
	return b.CommonBackend.insertRecover(
		insertRecoverSQL,
		projectID, start, stop, desc)
}

func (b *SQLiteBackend) insertProject(key, desc string) (int64, error) {
	return b.CommonBackend.insertProject(insertProjectSQL, key, desc)
}

func (b *SQLiteBackend) moveEntries(entries Entries, newID int64) error {
	return b.CommonBackend.moveEntries(moveEntrySQL, entries, newID)
}

func (b *SQLiteBackend) deleteEntries(entries Entries) error {
	return b.CommonBackend.deleteEntries(deleteProjectEntrySQL, entries)
}

func (b *SQLiteBackend) updateEntry(entry *Entry) error {
	return b.CommonBackend.updateEntry(updateEntrySQL, entry)
}

func (b *SQLiteBackend) updateProject(p *Project) error {
	return b.CommonBackend.updateProject(updateProjectSQL, p)
}

func (b *SQLiteBackend) joinEntries(desc string, entries Entries) error {
	return b.CommonBackend.joinEntries(
		joinSQL,
		deleteProjectEntrySQL,
		desc, entries)
}

func (b *SQLiteBackend) splitEntry(projectID int64, entry *Entry, duration time.Duration) (*Entry, error) {
	return b.CommonBackend.splitEntry(
		insertProjectEntrySQL,
		updateEntryEndSQL,
		projectID, entry, duration)
}
