package heco

import (
	"context"
	"database/sql"
	"fmt"
	"time"
)

const (
	deleteRecoverSQL = `DELETE FROM recover`

	loadActiveProjectsSQL = `
SELECT id, key, description
FROM projects
WHERE active
ORDER BY id`

	allProjectKeysSQL = `SELECT key FROM projects`
)

type CommonBackend struct {
	db *sql.DB
}

func (b *CommonBackend) Close() error {
	if b.db != nil {
		return b.db.Close()
	}
	return nil
}

func (b *CommonBackend) LoadProjects() (Projects, error) {
	var projects Projects

	ctx := context.Background()

	conn, err := b.db.Conn(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	rows, err := conn.QueryContext(ctx, loadActiveProjectsSQL)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var p Project
		if err := rows.Scan(&p.ID, &p.Key, &p.Desc); err != nil {
			return nil, err
		}

		if p.Key == "" {
			return nil, fmt.Errorf("project with id %d needs a key", p.ID)
		}
		projects = append(projects, &p)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	Infof("found %d active projects.\n", len(projects))

	return projects, nil
}

func (b *CommonBackend) LoadEntries(query string, projectID int64) (Entries, error) {

	ctx := context.Background()

	rows, err := b.db.QueryContext(
		ctx,
		query,
		projectID)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var entries Entries

	for rows.Next() {
		var (
			id         int64
			start, end time.Time
			desc       string
		)
		if err := rows.Scan(
			&id,
			&start,
			&end,
			&desc,
		); err != nil {
			return nil, err
		}
		entries = append(entries, NewEntry(
			id,
			start, end,
			desc))
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return entries, nil
}

func (b *CommonBackend) AllProjectKeys() ([]string, error) {
	ctx := context.Background()

	rows, err := b.db.QueryContext(ctx, allProjectKeysSQL)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var keys []string

	for rows.Next() {
		var key string
		if err := rows.Scan(&key); err != nil {
			return nil, err
		}
		keys = append(keys, key)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return keys, nil
}

func (b *CommonBackend) deleteEntries(deleteSQL string, entries Entries) error {
	if len(entries) == 0 {
		return nil
	}

	if err := func() error {
		ctx := context.Background()
		tx, err := b.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		defer tx.Rollback()

		stmt, err := tx.PrepareContext(ctx, deleteSQL)
		if err != nil {
			return err
		}

		for _, e := range entries {
			if _, err := stmt.ExecContext(ctx, e.ID); err != nil {
				return err
			}
		}

		return tx.Commit()
	}(); err != nil {
		return err
	}
	Debugf("deleted %d entries.\n", len(entries))
	return nil
}

func (b *CommonBackend) updateEntry(updateSQL string, entry *Entry) error {

	if err := func() error {
		ctx := context.Background()
		tx, err := b.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		defer tx.Rollback()

		if _, err := tx.ExecContext(
			ctx, updateSQL,
			entry.Desc, entry.Start, entry.End, entry.ID,
		); err != nil {
			return err
		}

		return tx.Commit()
	}(); err != nil {
		return err
	}
	Debugf("updated entry to '%s'.\n", entry)
	return nil
}

func (b *CommonBackend) updateProject(updateSQL string, p *Project) error {
	if p == nil {
		return nil
	}
	if err := func() error {
		ctx := context.Background()
		tx, err := b.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		defer tx.Rollback()

		if _, err := tx.ExecContext(
			ctx, updateSQL,
			p.Key, p.Desc, p.ID,
		); err != nil {
			return err
		}

		return tx.Commit()
	}(); err != nil {
		return err
	}
	Debugf("updated project: (%d) %s %s.\n",
		p.ID, p.Key, p.Desc)
	return nil
}

func (b *CommonBackend) joinEntries(
	joinSQL, deleteSQL,
	desc string,
	entries Entries,
) error {
	if len(entries) < 1 {
		return nil
	}

	var duration time.Duration
	for _, e := range entries {
		duration += e.Duration()
	}
	newEnd := entries[0].Start.Add(duration)

	if err := func() error {
		ctx := context.Background()
		tx, err := b.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		defer tx.Rollback()

		if _, err := tx.ExecContext(ctx, joinSQL, newEnd, desc, entries[0].ID); err != nil {
			return err
		}

		stmt, err := tx.PrepareContext(ctx, deleteSQL)
		if err != nil {
			return err
		}
		for _, e := range entries[1:] {
			if _, err := stmt.ExecContext(ctx, e.ID); err != nil {
				return err
			}
		}
		return tx.Commit()
	}(); err != nil {
		return err
	}

	entries[0].End = newEnd
	entries[0].Desc = desc

	return nil
}

func (b *CommonBackend) moveEntries(moveSQL string, entries Entries, newID int64) error {

	if len(entries) == 0 {
		return nil
	}

	if err := func() error {
		ctx := context.Background()
		tx, err := b.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		defer tx.Rollback()

		stmt, err := tx.PrepareContext(ctx, moveSQL)
		if err != nil {
			return err
		}

		for _, entry := range entries {
			if _, err := stmt.ExecContext(ctx, newID, entry.ID); err != nil {
				return err
			}
		}

		return tx.Commit()
	}(); err != nil {
		return err
	}
	Debugf("moved %d entries to project with id %d.\n",
		len(entries), newID)
	return nil
}

func (b *CommonBackend) insertProject(insertSQL, key, desc string) (int64, error) {
	if key == "" || desc == "" {
		return 0, nil
	}
	ctx := context.Background()
	con, err := b.db.Conn(ctx)
	if err != nil {
		return 0, err
	}
	defer con.Close()
	var id int64
	if err := con.QueryRowContext(ctx, insertSQL, key, desc).Scan(&id); err != nil {
		return 0, err
	}
	Debugf("Added a new project '%s' into db.\n", desc)
	return id, nil
}

func (b *CommonBackend) insertEntry(
	insertSQL string,
	projectID int64,
	start, stop time.Time,
	desc string,
) (int64, error) {
	ctx := context.Background()
	con, err := b.db.Conn(ctx)
	if err != nil {
		return 0, err
	}
	defer con.Close()

	tx, err := con.BeginTx(ctx, nil)
	if err != nil {
		return 0, err
	}

	var success bool
	defer func() {
		if !success {
			tx.Rollback()
		}
	}()
	var id int64

	if err := tx.QueryRowContext(ctx, insertSQL, projectID, start, stop, desc).Scan(&id); err != nil {
		return 0, err
	}
	if _, err := tx.ExecContext(ctx, deleteRecoverSQL); err != nil {
		return 0, err
	}
	if err := tx.Commit(); err != nil {
		return 0, err
	}
	success = true

	Debugf("added new entry '%s' to project '%d' into db.\n",
		desc, projectID)

	return id, nil
}

func (b *CommonBackend) splitEntry(
	insertSQL, updateSQL string,
	projectID int64, entry *Entry, duration time.Duration,
) (*Entry, error) {

	oldDuration := entry.Duration()
	newDuration := oldDuration - duration

	newEnd := entry.Start.Add(newDuration)
	newEntryEnd := newEnd.Add(duration)

	ctx := context.Background()
	con, err := b.db.Conn(ctx)
	if err != nil {
		return nil, err
	}
	defer con.Close()

	tx, err := con.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	var success bool
	defer func() {
		if !success {
			tx.Rollback()
		}
	}()

	var id int64
	if err := tx.QueryRowContext(ctx, insertSQL,
		projectID, newEnd, newEntryEnd, entry.Desc,
	).Scan(&id); err != nil {
		return nil, err
	}

	if _, err := tx.ExecContext(ctx, updateSQL, newEnd, entry.ID); err != nil {
		return nil, err
	}
	if err := tx.Commit(); err != nil {
		return nil, err
	}
	success = true

	entry.End = newEnd

	return NewEntry(id, newEnd, newEntryEnd, entry.Desc), nil
}

func (b *CommonBackend) insertRecover(
	insertSQL string,
	projectID int64,
	start, stop time.Time,
	desc string,
) error {
	ctx := context.Background()

	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if _, err := tx.ExecContext(ctx, insertSQL,
		projectID, start, stop, desc,
	); err != nil {
		return err
	}

	return tx.Commit()
}

func (b *CommonBackend) LoadRecover(
	loadSQL, insertSQL string,
) error {
	ctx := context.Background()

	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	var (
		id, projectID int64
		start, stop   time.Time
		desc          string
	)

	switch err := tx.QueryRowContext(ctx, loadSQL).Scan(
		&id, &projectID,
		&start, &stop,
		&desc,
	); {
	case err == sql.ErrNoRows:
		return nil

	case err != nil:
		return err
	}

	if _, err := tx.ExecContext(ctx, insertSQL,
		projectID, start, stop, desc,
	); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, deleteRecoverSQL); err != nil {
		return err
	}

	return tx.Commit()
}
