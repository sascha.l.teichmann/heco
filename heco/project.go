// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/rivo/tview"
)

type ProjectMode int

const (
	dayMode = ProjectMode(iota)
	weekMode
	lastWeekMode
	monthMode
	yearMode
	totalMode
)

type ProjectSortMode int

const (
	idSortMode = ProjectSortMode(iota)
	keySortMode
	descSortMode
	treeSortMode
)

type Entry struct {
	ID    int64
	Start time.Time
	End   time.Time
	Desc  string
}

type Entries []*Entry

type Project struct {
	ID   int64
	Key  string
	Desc string

	totalMode ProjectMode
	total     time.Duration

	Entries Entries
}

type Projects []*Project

func (pm ProjectMode) Cycle() ProjectMode {
	if pm == totalMode {
		return dayMode
	}
	return pm + 1
}

func parseProjectMode(s string) ProjectMode {
	switch strings.ToLower(s) {
	case "day":
		return dayMode
	case "week":
		return weekMode
	case "last_week":
		return lastWeekMode
	case "month":
		return monthMode
	case "year":
		return yearMode
	case "total":
		return totalMode
	default:
		return -1
	}
}

func (ps Projects) sortBy(mode ProjectSortMode) Projects {
	n := make(Projects, len(ps))
	copy(n, ps)
	var less func(i, j int) bool
	switch mode {
	case idSortMode:
		less = func(i, j int) bool { return n[i].ID < n[j].ID }
	case keySortMode:
		less = func(i, j int) bool { return n[i].Key < n[j].Key }
	case descSortMode:
		less = func(i, j int) bool { return n[i].Desc < n[j].Desc }
	case treeSortMode:
		less = func(i, j int) bool { return n[i].Key < n[j].Key }
	default:
		return n
	}
	sort.Slice(n, less)
	return n
}

func parseProjectSort(s string) ProjectSortMode {
	switch s {
	case "id":
		return idSortMode
	case "key":
		return keySortMode
	case "desc":
		return descSortMode
	case "tree":
		return treeSortMode
	default:
		return -1
	}
}

func (ps Projects) Total() time.Duration {
	var sum time.Duration
	for _, p := range ps {
		sum += p.Total()
	}
	return sum
}

func (ps Projects) insert(p *Project) Projects {

	n := len(ps)
	if n == 0 {
		return Projects{p}
	}

	// It is assumed that the projects are sorted in ID order.
	idx := sort.Search(n, func(i int) bool { return ps[i].ID >= p.ID })

	if idx < n && ps[idx].ID == p.ID {
		// Replace existing.
		ps[idx] = p
		return ps
	}

	if cap(ps) > n {
		// Still room.
		ps = ps[:n+1]
		copy(ps[idx+1:], ps[idx:])
		ps[idx] = p
		return ps
	}

	// Allocate new
	nps := make(Projects, n+1, n+32)
	if idx > 0 {
		copy(nps, ps[:idx])
	}
	nps[idx] = p
	if idx < n {
		copy(nps[idx+1:], ps[idx:])
	}
	return nps
}

func (ps Projects) Duration(mode ProjectMode) time.Duration {
	var duration time.Duration
	for _, p := range ps {
		duration += p.Duration(mode)
	}
	return duration
}

func (es Entries) Duration(accept func(*Entry) bool) time.Duration {
	var sum time.Duration
	for _, e := range es {
		if accept(e) {
			sum += e.Duration()
		}
	}
	return sum
}

func (es Entries) Total() time.Duration {
	return es.Duration(func(*Entry) bool { return true })
}

func (p *Project) Duration(m ProjectMode) time.Duration {
	if m != p.totalMode {
		accept := m.Accept(time.Now())
		p.total = p.Entries.Duration(accept)
		p.totalMode = m
	}
	return p.total
}

func (p *Project) Total() time.Duration {
	return p.Duration(totalMode)
}

/* This is old code from templating. TODO: Remove?

func (p *Project) Year() time.Duration {
	return p.Duration(yearMode)
}

func (p *Project) Month() time.Duration {
	return p.Duration(monthMode)
}

func (p *Project) Week() time.Duration {
	return p.Duration(weekMode)
}

func (p *Project) Day() time.Duration {
	return p.Duration(dayMode)
}
*/

func (p *Project) GetEntries(backend Backend) (Entries, error) {
	if p == nil {
		return nil, nil
	}
	if p.Entries == nil {
		entries, err := backend.LoadEntries(p.ID)
		if err != nil {
			return nil, err
		}
		if entries == nil {
			entries = Entries{}
		}
		p.Entries = entries
		p.totalMode = -1
	}
	return p.Entries, nil
}

func (p *Project) AddEntry(entry *Entry) {
	p.Entries = p.Entries.insert(entry)
	p.totalMode = -1
}

func (p *Project) Merge(entries Entries) {
	p.Entries = p.Entries.merge(entries)
	p.totalMode = -1
}

func (p *Project) LoadEntries(backend *SQLiteBackend) error {
	entries, err := backend.LoadEntries(p.ID)
	if err != nil {
		return err
	}
	p.Entries = entries
	return nil
}

func NewEntry(
	id int64,
	start, end time.Time,
	desc string,
) *Entry {
	return &Entry{
		ID:    id,
		Start: start,
		End:   end,
		Desc:  desc,
	}
}

/* This is old code from templating. TODO: Remove?

var workPackageRe = regexp.MustCompile(`^\[([^\s\]]+)(\s|\])`)

func (e *Entry) WorkPackage() string {
	if m := workPackageRe.FindStringSubmatchIndex(e.Desc); m != nil {
		return e.Desc[m[2]:m[3]]
	}
	return "-"
}

func (e *Entry) Comment() string {
	comment := e.Desc
	if comment != "" {
		if m := workPackageRe.FindStringSubmatchIndex(comment); m != nil {
			comment = strings.TrimSpace(comment[m[1]:])
		}
		comment = strings.Replace(comment, "\x1b", "", -1)
	}
	return comment
}

func (e *Entry) ISOFormat() string {
	return e.Start.Format(datetimeFormat)
}
*/

func (e *Entry) Duration() time.Duration {
	return e.End.Sub(e.Start)
}

func (e *Entry) SelectedText(selected bool) string {
	var sb strings.Builder
	if selected {
		sb.WriteString("> ")
	} else {
		sb.WriteString("  ")
	}
	sb.WriteString(formatDatetime(e.Start))
	sb.WriteString(" [")
	sb.WriteString(ShortTime(e.Duration()))
	sb.WriteString("h] ")
	sb.WriteString(tview.Escape(e.Desc))
	return sb.String()
}

func (e *Entry) String() string {
	return fmt.Sprintf("[%d | %s | %s | %s]",
		e.ID, e.Start, e.End, e.Desc)
}

func (pm ProjectMode) String() string {
	switch pm {
	case totalMode:
		return "total"
	case yearMode:
		return "year"
	case monthMode:
		return "month"
	case lastWeekMode:
		return "last_week"
	case weekMode:
		return "week"
	case dayMode:
		return "day"
	default:
		return "unknown"
	}
}

func (pm ProjectMode) Accept(t time.Time) func(*Entry) bool {
	switch pm {
	case dayMode:
		year, month, day := t.Date()
		return func(e *Entry) bool {
			y, m, d := e.Start.Date()
			if y == year && m == month && d == day {
				return true
			}
			y, m, d = e.End.Date()
			return y == year && m == month && d == day
		}
	case weekMode:
		year, week := t.ISOWeek()
		return func(e *Entry) bool {
			y, w := e.Start.ISOWeek()
			if y == year && w == week {
				return true
			}
			y, w = e.End.ISOWeek()
			return y == year && w == week
		}
	case lastWeekMode:
		year, week := t.AddDate(0, 0, -7).ISOWeek()
		return func(e *Entry) bool {
			y, w := e.Start.ISOWeek()
			if y == year && w == week {
				return true
			}
			y, w = e.End.ISOWeek()
			return y == year && w == week
		}
	case monthMode:
		year, month, _ := t.Date()
		return func(e *Entry) bool {
			y, m, _ := e.Start.Date()
			if y == year && m == month {
				return true
			}
			y, m, _ = e.End.Date()
			return y == year && m == month
		}
	case yearMode:
		year := t.Year()
		return func(e *Entry) bool {
			return e.Start.Year() == year || e.End.Year() == year
		}
	default:
		return func(*Entry) bool { return true }
	}
}

func (es Entries) walk(accept func(*Entry) bool, visit func(*Entry)) {
	for _, e := range es {
		if accept(e) {
			visit(e)
		}
	}
}

func (es Entries) nTh(accept func(*Entry) bool, n int) *Entry {
	var count int
	for _, e := range es {
		if accept(e) {
			if count == n {
				return e
			}
			count++
		}
	}
	return nil
}

func (p *Project) deleteEntries(
	accept func(*Entry) bool,
	selected map[int]struct{},
	visit func(*Entry),
) {
	p.Entries = p.Entries.delete(accept, selected, visit)
	p.totalMode = -1
}

func (p *Project) removeEntries(entries Entries) {
	p.Entries = p.Entries.remove(entries)
	p.totalMode = -1
}

func (es Entries) delete(
	accept func(*Entry) bool,
	selected map[int]struct{},
	visit func(*Entry),
) Entries {

	if len(selected) == 0 {
		return es
	}

	var indices []int

	var count int
	for i, e := range es {
		if accept(e) {
			if _, ok := selected[count]; ok {
				visit(e)
				if indices = append(indices, i); len(indices) == len(selected) {
					break
				}
			}
			count++
		}
	}

	for i := len(indices) - 1; i >= 0; i-- {
		idx := indices[i]
		if idx < len(es)-1 {
			copy(es[idx:], es[idx+1:])
			es[len(es)-1] = nil
		}
		es = es[:len(es)-1]
	}

	return es
}

func (es Entries) merge(other Entries) Entries {
	for _, e := range other {
		es = es.insert(e)
	}
	return es
}

func (es Entries) insert(entry *Entry) Entries {

	n := len(es)
	if n == 0 {
		return Entries{entry}
	}

	// It is assumed that the entries are sorted in descending start order.
	idx := sort.Search(n, func(i int) bool { return !entry.Start.Before(es[i].Start) })

	if cap(es) > n {
		// Still room.
		es = es[:n+1]
		copy(es[idx+1:], es[idx:])
		es[idx] = entry
		return es
	}

	// Allocate new
	nes := make(Entries, n+1, n+32)
	if idx > 0 {
		copy(nes, es[:idx])
	}
	nes[idx] = entry
	if idx < n {
		copy(nes[idx+1:], es[idx:])
	}
	return nes
}

func (es Entries) remove(toRemove Entries) Entries {
	x := make(map[*Entry]struct{})
	for _, e := range toRemove {
		x[e] = struct{}{}
	}
	out := es[:0]
	for i, e := range es {
		if _, ok := x[e]; ok {
			es[i] = nil
		} else {
			out = append(out, e)
		}
	}
	return out
}
