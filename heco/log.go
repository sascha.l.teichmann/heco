// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"log"
	"os"
	"strings"
	"sync"
	"sync/atomic"
)

type LogLevel int32

const (
	TraceLogLevel LogLevel = iota
	DebugLogLevel
	InfoLogLevel
	WarnLogLevel
	ErrorLogLevel
	FatalLogLevel
)

var (
	logLevel  = uint32(InfoLogLevel)
	logFileMu sync.Mutex
	logFile   *os.File
)

func SetupLog(filename string, perm os.FileMode) error {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, perm)
	if err != nil {
		return err
	}
	logFileMu.Lock()
	defer logFileMu.Unlock()
	if logFile != nil {
		logFile.Close()
	}
	logFile = f
	log.SetOutput(logFile)
	return nil
}

func ShutdownLog() {
	logFileMu.Lock()
	defer logFileMu.Unlock()
	if logFile != nil {
		logFile.Close()
		logFile = nil
	}
	log.SetOutput(os.Stderr)
}

func ParseLogLevel(s string) LogLevel {
	switch strings.ToLower(s) {
	case "trace":
		return TraceLogLevel
	case "debug":
		return DebugLogLevel
	case "info":
		return InfoLogLevel
	case "warn":
		return WarnLogLevel
	case "error":
		return ErrorLogLevel
	case "fatal":
		return FatalLogLevel
	default:
		return InfoLogLevel
	}
}

func (level LogLevel) String() string {
	switch level {
	case TraceLogLevel:
		return "trace"
	case DebugLogLevel:
		return "debug"
	case InfoLogLevel:
		return "info"
	case WarnLogLevel:
		return "warn"
	case ErrorLogLevel:
		return "error"
	case FatalLogLevel:
		return "fatal"
	default:
		return "unknown"
	}
}

func GetLogLevel() LogLevel {
	return LogLevel(atomic.LoadUint32(&logLevel))
}

func SetLogLevel(level LogLevel) {
	atomic.StoreUint32(&logLevel, uint32(level))
}

func Tracef(fmt string, args ...interface{}) {
	if TraceLogLevel >= GetLogLevel() {
		log.Printf("[TRACE] "+fmt, args...)
	}
}

func Debugf(fmt string, args ...interface{}) {
	if DebugLogLevel >= GetLogLevel() {
		log.Printf("[DEBUG] "+fmt, args...)
	}
}

func Infof(fmt string, args ...interface{}) {
	if InfoLogLevel >= GetLogLevel() {
		log.Printf("[INFO] "+fmt, args...)
	}
}

func Warnf(fmt string, args ...interface{}) {
	if WarnLogLevel >= GetLogLevel() {
		log.Printf("[WARN] "+fmt, args...)
	}
}

func Errorf(fmt string, args ...interface{}) {
	if ErrorLogLevel >= GetLogLevel() {
		log.Printf("[ERROR] "+fmt, args...)
	}
}

func Fatalf(fmt string, args ...interface{}) {
	if FatalLogLevel >= GetLogLevel() {
		log.Fatalf("[FATAL] "+fmt, args...)
	}
}
