// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

// Package heco implements the central logic of heco.
package heco
