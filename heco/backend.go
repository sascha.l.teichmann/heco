// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"context"
	"strings"
	"time"
)

type Backend interface {
	Close() error
	LoadProjects() (Projects, error)
	LoadRecover() error
	LoadEntries(projectID int64) (Entries, error)
	AllProjectKeys() ([]string, error)

	insertEntry(projectID int64, start, stop time.Time, desc string) (int64, error)
	insertRecover(projectID int64, start, stop time.Time, desc string) error
	updateProject(p *Project) error
	insertProject(key, desc string) (int64, error)
	splitEntry(projectID int64, entry *Entry, duration time.Duration) (*Entry, error)
	joinEntries(desc string, entries Entries) error
	moveEntries(entries Entries, newID int64) error
	updateEntry(entry *Entry) error
	deleteEntries(entries Entries) error
}

func NewBackend(ctx context.Context, database string) (Backend, error) {
	if strings.HasPrefix(database, "postgresql:") {
		return NewPGBackend(ctx, database)
	}
	return NewSQLiteBackend(ctx, database)
}
