// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gdamore/tcell/v2"
)

// State is function that handles input.
type State func(ctrl *Controller, event *tcell.EventKey) (State, error)

// ProjectState is the function that handles the input of the projects.
func ProjectState(ctrl *Controller, event *tcell.EventKey) (State, error) {
	Debugf("project state\n")
	switch keys := &ctrl.config.Keys; {

	case keys.SwitchTimeMode.is(event):
		return nil, ctrl.view.projects.switchTimeMode()

	case keys.SwitchProjectOrder.is(event):
		return nil, ctrl.view.projects.switchProjectOrder()

	case keys.SwitchLists.is(event):
		if ctrl.project != nil {
			ctrl.view.footer.info(
				ctrl.config.theme.Info,
				Translate("choose.entries"))
			ctrl.app.SetFocus(ctrl.view.entries.list)
			return EntriesState, nil
		}
		return nil, nil

	case keys.Escape.is(event):
		return reallyQuit(ctrl, Translate("choose.project"), ProjectState), nil

	case keys.Enter.is(event):
		if ctrl.project != nil && !ctrl.running {
			ctrl.showRunning(0)
			tt := ctrl.startTimeTaking(event.When())
			ctrl.running = true
			return tt.running, nil
		}
		return nil, nil

	case keys.ProjectEdit.is(event):
		return editProject(ctrl)

	case keys.Insert.is(event):
		return insertProject(ctrl)

	default:
		prefix := string(event.Rune())
		for _, p := range ctrl.projects {
			if strings.HasPrefix(p.Key, prefix) {
				ctrl.view.projects.onSelected(ctrl.startProject, nil)
				ctrl.view.projects.chooser.
					SetLabel(Translate("selected.project") + " ").
					SetText("")
				ctrl.view.projects.selector.SwitchToPage("chooser")
				ctrl.app.SetFocus(ctrl.view.projects.chooser)
				ctrl.app.QueueEvent(event)
				return IgnoreState, nil
			}
		}
	}

	return nil, nil
}

func backToEntries(ctrl *Controller) {
	// Restore UI
	ctrl.view.rightSelector.SwitchToPage("entries")
	ctrl.app.SetFocus(ctrl.view.projects.table)
	ctrl.state = ProjectState
}

func editProject(ctrl *Controller) (State, error) {
	p := ctrl.project
	if p == nil {
		return nil, nil
	}

	edit := func(key, desc string, ok bool) {
		defer backToEntries(ctrl)

		if !ok || (key == p.Key && p.Desc == desc) {
			return
		}
		p.Key, p.Desc, key, desc = key, desc, p.Key, p.Desc
		if err := ctrl.backend.updateProject(p); err != nil {
			Errorf("updating project failed: %v\n", err)
			p.Key, p.Desc = key, desc
			return
		}
		if err := ctrl.view.projects.buildTable(); err != nil {
			Errorf("Building table failed: %v\n", err)
		}
	}

	if err := ctrl.view.projectMeta.editProject(
		Translate("edit.project"),
		p.Key, p.Desc,
		edit,
	); err != nil {
		Errorf("edit project failed: %v\n", err)
		return nil, nil
	}
	return IgnoreState, nil
}

func insertProject(ctrl *Controller) (State, error) {

	insert := func(key, desc string, ok bool) {
		defer backToEntries(ctrl)

		if !ok || key == "" {
			return
		}

		id, err := ctrl.backend.insertProject(key, desc)
		if err != nil {
			Errorf("failed to create new project: %v\n", err)
		}

		p := &Project{
			ID:        id,
			Key:       key,
			Desc:      desc,
			totalMode: -1,
		}

		ctrl.projects = ctrl.projects.insert(p)
		ctrl.project = p

		if err := ctrl.view.projects.buildTable(); err != nil {
			Errorf("failed to build projects table: %v\n", err)
		}

		ctrl.view.projects.selectProject()
	}

	if err := ctrl.view.projectMeta.editProject(
		Translate("insert.project"),
		"", "",
		insert,
	); err != nil {
		Errorf("edit project failed: %v\n", err)
		return nil, nil
	}
	return IgnoreState, nil
}

// IgnoreState is a function that does not handle inputs.
func IgnoreState(*Controller, *tcell.EventKey) (State, error) {
	return nil, nil
}

// EntriesState handles the input in the events window.
func EntriesState(ctrl *Controller, event *tcell.EventKey) (State, error) {
	switch keys := &ctrl.config.Keys; {

	case keys.SwitchLists.is(event):
		ctrl.view.footer.info(
			ctrl.config.theme.Info,
			Translate("choose.project"))
		ctrl.app.SetFocus(ctrl.view.projects.table)
		return ProjectState, nil

	case keys.EntryInsert.is(event):
		return insertEntry(ctrl)

	case keys.EntryDelete.is(event):
		return delEntries(ctrl)

	case keys.EntryEdit.is(event):
		return editEntryText(ctrl)

	case keys.EntryLength.is(event):
		return editEntryLength(ctrl)

	case keys.EntryAdjust.is(event):
		return editEntryStart(ctrl)

	case keys.EntryMove.is(event):
		return moveEntries(ctrl)

	case keys.EntryJoin.is(event):
		return joinEntries(ctrl)

	case keys.EntrySplit.is(event):
		return splitEntry(ctrl)

	case keys.Escape.is(event):
		return reallyQuit(ctrl, Translate("choose.entries"), EntriesState), nil
	}

	return nil, nil
}

func backToProjects(ctrl *Controller) {
	// Restore UI
	ctrl.view.leftSelector.SwitchToPage("projects")
	ctrl.app.SetFocus(ctrl.view.entries.list)
	ctrl.state = EntriesState
}

func insertEntry(ctrl *Controller) (State, error) {
	if ctrl.project == nil {
		return nil, nil
	}

	var entry Entry

	if ctrl.view.entries.list.GetItemCount() > 0 {
		idx := ctrl.view.entries.list.GetCurrentItem()
		e := ctrl.project.Entries.nTh(ctrl.getModeFilter(), idx)
		if e != nil {
			entry.Start = e.End
		} else {
			entry.Start = time.Now()
		}
	} else {
		entry.Start = time.Now()
	}
	entry.End = entry.Start

	newEntry := func(e *Entry, ok bool) {
		defer backToProjects(ctrl)

		if !ok {
			return
		}
		id, err := ctrl.backend.insertEntry(
			ctrl.project.ID,
			e.Start,
			e.End,
			e.Desc)
		if err != nil {
			Errorf("Inserting new failed: %v\n", err)
			return
		}
		e.ID = id
		ctrl.project.AddEntry(e)

		if err := ctrl.view.entries.fillEntries(); err != nil {
			Errorf("displaying entries failed: %v\n", err)
			return
		}

		// Select the new entry.
		ctrl.view.entries.setCurrentEntry(e, ctrl.getModeFilter())

		// Add time to project.
		ctrl.view.projects.updateProjectTime(ctrl.project)

		// Update all times.
		ctrl.view.projects.allProjectsTime()
	}

	ctrl.view.entryMeta.editEntry(
		Translate("new.entry"),
		&entry,
		newEntry)

	return IgnoreState, nil
}

func delEntries(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	ctrl.view.entries.info(
		ctrl.config.theme.Question,
		Translate("really.delete.entry"))
	q := question{
		yes: deleteEntries,
		no: func(crtl *Controller) (State, error) {
			crtl.view.entries.info(ctrl.config.theme.EntryFooter, "")
			return EntriesState, nil
		},
	}
	return q.state, nil
}

func splitEntry(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	idx := ctrl.view.entries.list.GetCurrentItem()
	entry := ctrl.project.Entries.nTh(ctrl.getModeFilter(), idx)
	if entry == nil {
		return nil, nil
	}

	length := entry.End.Sub(entry.Start)
	// Nothing to split if it iss shorter than a second.
	if length <= time.Second {
		return nil, nil
	}

	ctrl.view.entries.question.SetDoneFunc(func(key tcell.Key) {
		text := ctrl.view.entries.question.GetText()
		ctrl.view.entries.question.SetDoneFunc(nil)
		ctrl.view.entries.question.SetAcceptanceFunc(nil)
		ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")
		ctrl.app.SetFocus(ctrl.view.entries.list)
		ctrl.state = EntriesState

		if key != tcell.KeyEnter {
			return
		}

		duration, err := parseTimeLength(text)
		if err != nil {
			Errorf("parsing time duration failed: %v\n", err)
			return
		}

		// Not a split
		if time.Second > duration || duration >= length {
			return
		}

		newEntry, err := ctrl.backend.splitEntry(ctrl.project.ID, entry, duration)
		if err != nil {
			Errorf("splitting entry failed: %v\n", err)
			return
		}

		ctrl.project.AddEntry(newEntry)

		if err := ctrl.view.entries.fillEntries(); err != nil {
			Errorf("displaying entries failed: %v\n", err)
			return
		}

		// Select the new entry.
		if filter := ctrl.getModeFilter(); !ctrl.view.entries.setCurrentEntry(newEntry, filter) {
			// Should not happen
			ctrl.view.entries.setCurrentEntry(entry, filter)
		}
	})

	ctrl.view.entries.question.SetAcceptanceFunc(acceptTimeLength)
	ctrl.view.entries.ask(
		ctrl.config.theme.Question,
		Translate("split.entry")+" ",
		HumanTime(length))

	return IgnoreState, nil
}

func joinEntries(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	// Clone as we modify it with the currently selected row.
	selected := make(map[int]struct{})
	for k := range ctrl.view.entries.selected {
		selected[k] = struct{}{}
	}

	selected[ctrl.view.entries.list.GetCurrentItem()] = struct{}{}

	// Something to join?
	if len(selected) < 2 {
		return nil, nil
	}

	filter := ctrl.getModeFilter()
	var entries Entries
	for i := range selected {
		if entry := ctrl.project.Entries.nTh(filter, i); entry != nil {
			entries = append(entries, entry)
		}
	}

	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Start.Before(entries[j].Start)
	})

	var descs []string
	dupes := make(map[string]struct{})
	for _, e := range entries {
		if _, ok := dupes[e.Desc]; !ok {
			dupes[e.Desc] = struct{}{}
			descs = append(descs, e.Desc)
		}
	}

	desc := strings.Join(descs, ". ")

	ctrl.view.entries.question.SetDoneFunc(func(key tcell.Key) {
		text := ctrl.view.entries.question.GetText()
		ctrl.view.entries.question.SetDoneFunc(nil)
		ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")
		ctrl.app.SetFocus(ctrl.view.entries.list)
		ctrl.state = EntriesState

		if key != tcell.KeyEnter {
			return
		}

		if err := ctrl.backend.joinEntries(text, entries); err != nil {
			Errorf("joining entries failed: %v\n", err)
			return
		}
		ctrl.project.removeEntries(entries[1:])

		if err := ctrl.view.entries.fillEntries(); err != nil {
			Errorf("displaying entries failed: %v\n", err)
			return
		}

		// Select the joined entry.
		ctrl.view.entries.setCurrentEntry(entries[0], filter)
	})

	ctrl.view.entries.question.SetAcceptanceFunc(nil)
	ctrl.view.entries.ask(
		ctrl.config.theme.Question,
		Translate("join.entry")+" ",
		desc)

	return IgnoreState, nil
}

func moveEntries(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	moveTo := func(dest *Project) State {
		// Clone as we modify it with the currently selected row.
		selected := make(map[int]struct{})
		for k := range ctrl.view.entries.selected {
			selected[k] = struct{}{}
		}

		selected[ctrl.view.entries.list.GetCurrentItem()] = struct{}{}

		var removed Entries

		ctrl.project.deleteEntries(
			ctrl.getModeFilter(),
			selected,
			func(e *Entry) { removed = append(removed, e) })

		if err := ctrl.backend.moveEntries(removed, dest.ID); err != nil {
			// Fill entries back in the original project.
			ctrl.project.Merge(removed)
			ctrl.app.SetFocus(ctrl.view.entries.list)
			return EntriesState
		}

		dest.Merge(removed)

		ctrl.view.projects.updateProjectTime(ctrl.project)
		ctrl.view.projects.updateProjectTime(dest)

		if err := ctrl.view.entries.fillEntries(); err != nil {
			Errorf("displaying entries failed: %v\n", err)
		}
		ctrl.view.projects.allProjectsTime()

		ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")

		ctrl.app.SetFocus(ctrl.view.entries.list)
		return EntriesState
	}

	ctrl.view.projects.onSelected(
		func(project *Project) {
			Debugf("selected project '%s'\n", project.Desc)

			ctrl.view.footer.info(
				ctrl.config.theme.Info,
				Translate("choose.entries"))

			ctrl.view.footer.info(
				ctrl.config.theme.Question,
				Translate("are.you.sure"))

			ctrl.app.SetFocus(ctrl.view.footer.information)

			q := question{
				yes: func(ctrl *Controller) (State, error) {
					ctrl.view.footer.info(
						ctrl.config.theme.Info,
						Translate("choose.entries"))
					ctrl.app.SetFocus(ctrl.view.entries.list)
					return moveTo(project), nil
				},
				no: func(*Controller) (State, error) {
					return EntriesState, nil
				},
			}
			ctrl.state = q.state
		},
		func() {
			Debugf("cancelled\n")
			ctrl.view.footer.info(
				ctrl.config.theme.Info,
				Translate("choose.entries"))
			ctrl.state = EntriesState
		})

	ctrl.view.projects.chooser.
		SetLabel(Translate("move.into.which") + " ").
		SetText("")

	ctrl.view.projects.chooser.Autocomplete()

	ctrl.view.projects.selector.SwitchToPage("chooser")
	ctrl.app.SetFocus(ctrl.view.projects.chooser)

	return IgnoreState, nil
}

func editEntryStart(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	idx := ctrl.view.entries.list.GetCurrentItem()
	entry := ctrl.project.Entries.nTh(ctrl.getModeFilter(), idx)
	if entry == nil {
		return nil, nil
	}

	ctrl.view.entries.question.SetDoneFunc(func(key tcell.Key) {
		text := ctrl.view.entries.question.GetText()
		ctrl.view.entries.question.SetDoneFunc(nil)
		ctrl.view.entries.question.SetAcceptanceFunc(nil)
		ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")
		ctrl.app.SetFocus(ctrl.view.entries.list)
		ctrl.state = EntriesState

		if key != tcell.KeyEnter {
			return
		}

		start, err := parseDate(text)
		if err != nil {
			Errorf("parsing date failed: %v\n", err)
			return
		}

		delta := start.Sub(entry.Start)
		entry.Start = start
		entry.End = entry.End.Add(delta)
		if err := ctrl.backend.updateEntry(entry); err != nil {
			Errorf("updating entry failed: %v\n", err)
			entry.Start = entry.Start.Add(-delta)
			entry.End = entry.End.Add(-delta)
			return
		}

		ctrl.project.totalMode = -1
		ctrl.view.projects.allProjectsTime()

		// If it got moved it may fall out of the filter.
		fixupEntries(ctrl, entry, idx)
	})

	ctrl.view.entries.question.SetAcceptanceFunc(acceptDate)
	ctrl.view.entries.ask(
		ctrl.config.theme.Question,
		Translate("edit.entry.start")+" ",
		entry.Start.Format("2006-01-02 15:04:05"))

	return IgnoreState, nil
}

func editEntryLength(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	idx := ctrl.view.entries.list.GetCurrentItem()
	entry := ctrl.project.Entries.nTh(ctrl.getModeFilter(), idx)
	if entry == nil {
		return nil, nil
	}

	length := entry.End.Sub(entry.Start)

	ctrl.view.entries.question.SetDoneFunc(func(key tcell.Key) {
		text := ctrl.view.entries.question.GetText()
		ctrl.view.entries.question.SetDoneFunc(nil)
		ctrl.view.entries.question.SetAcceptanceFunc(nil)
		ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")
		ctrl.app.SetFocus(ctrl.view.entries.list)
		ctrl.state = EntriesState

		if key != tcell.KeyEnter {
			return
		}

		duration, err := parseTimeLength(text)
		if err != nil {
			Errorf("parsing time duration failed: %v\n", err)
			return
		}

		oldEnd := entry.End
		entry.End = entry.Start.Add(duration)
		if err := ctrl.backend.updateEntry(entry); err != nil {
			Errorf("updating entry failed: %v\n", err)
			entry.End = oldEnd
			return
		}
		ctrl.project.totalMode = -1
		ctrl.view.projects.allProjectsTime()

		// If it got shorter it may fall out of the filter.
		fixupEntries(ctrl, entry, idx)
	})

	ctrl.view.entries.question.SetAcceptanceFunc(acceptTimeLength)
	ctrl.view.entries.ask(
		ctrl.config.theme.Question,
		Translate("edit.entry.length")+" ",
		HumanTimeFormatted(length, "00:00:00"))

	return IgnoreState, nil
}

func fixupEntries(ctrl *Controller, entry *Entry, idx int) {
	if ctrl.getModeFilter()(entry) { // Still visible?
		_, selected := ctrl.view.entries.selected[idx]
		text := entry.SelectedText(selected)
		ctrl.view.entries.list.SetItemText(idx, text, "")
		ctrl.view.projects.updateProjectTime(ctrl.project)
	} else { // Thrown out -> rebuild.
		if err := ctrl.view.entries.fillEntries(); err != nil {
			Errorf("build entries failed: %v\n", err)
		}
		// If nothing left switch back to projects.
		if ctrl.view.entries.list.GetItemCount() == 0 {
			ctrl.view.footer.info(
				ctrl.config.theme.Info,
				Translate("choose.project"))
			ctrl.app.SetFocus(ctrl.view.projects.table)
			ctrl.state = ProjectState
		}
	}
}

func editEntryText(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	idx := ctrl.view.entries.list.GetCurrentItem()
	entry := ctrl.project.Entries.nTh(ctrl.getModeFilter(), idx)
	if entry == nil {
		return nil, nil
	}

	ctrl.view.entries.question.SetDoneFunc(func(key tcell.Key) {
		text := ctrl.view.entries.question.GetText()
		ctrl.view.entries.question.SetDoneFunc(nil)
		ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")
		ctrl.app.SetFocus(ctrl.view.entries.list)
		ctrl.state = EntriesState

		if key == tcell.KeyEnter && text != entry.Desc {
			old := entry.Desc
			entry.Desc = text
			if err := ctrl.backend.updateEntry(entry); err != nil {
				entry.Desc = old
				Errorf("updating entry failed: %v\n", err)
				return
			}
			_, selected := ctrl.view.entries.selected[idx]
			text := entry.SelectedText(selected)
			ctrl.view.entries.list.SetItemText(idx, text, "")
		}
	})

	ctrl.view.entries.ask(
		ctrl.config.theme.Question,
		Translate("edit.entry.text")+" ",
		entry.Desc)

	return IgnoreState, nil
}

func deleteEntries(ctrl *Controller) (State, error) {
	if ctrl.view.entries.list.GetItemCount() == 0 {
		return nil, nil
	}

	// Clone as we modify it with the currently selected row.
	selected := make(map[int]struct{})
	for k := range ctrl.view.entries.selected {
		selected[k] = struct{}{}
	}

	selected[ctrl.view.entries.list.GetCurrentItem()] = struct{}{}

	var removed Entries

	ctrl.project.deleteEntries(
		ctrl.getModeFilter(),
		selected,
		func(e *Entry) { removed = append(removed, e) })

	if err := ctrl.backend.deleteEntries(removed); err != nil {
		Errorf("deleting entries failed: %v\n", err)
		// Put deleted entries back into project.
		ctrl.project.Merge(removed)
		return EntriesState, nil
	}

	ctrl.view.projects.updateProjectTime(ctrl.project)

	if err := ctrl.view.entries.fillEntries(); err != nil {
		Errorf("displaying entries failed: %v\n", err)
	}
	ctrl.view.projects.allProjectsTime()

	ctrl.view.entries.info(ctrl.config.theme.EntryFooter, "")

	ctrl.app.SetFocus(ctrl.view.entries.list)
	return EntriesState, nil
}

func reallyQuit(ctrl *Controller, returnText string, returnState State) State {
	ctrl.view.footer.info(
		ctrl.config.theme.Question,
		Translate("really.quit"))
	q := question{
		yes: func(ctrl *Controller) (State, error) {
			ctrl.quit()
			return IgnoreState, nil
		},
		no: func(crtl *Controller) (State, error) {
			crtl.view.footer.info(ctrl.config.theme.Info, returnText)
			return returnState, nil
		},
	}
	return q.state
}

type question struct {
	yes func(*Controller) (State, error)
	no  func(*Controller) (State, error)
}

func (q *question) state(ctrl *Controller, event *tcell.EventKey) (State, error) {

	switch event.Rune() {
	case 'y', 'Y', 'j', 'J':
		return q.yes(ctrl)
	case 'n', 'N':
		return q.no(ctrl)
	}

	// Swallow all other keys.
	return q.state, nil
}

type timeTaking struct {
	start       time.Time
	pause       time.Time
	totalPauses time.Duration
	lastSave    int
	ctrl        *Controller
	ticker      *time.Ticker
	fnCh        chan func(*timeTaking)
	done        bool
}

func (tt *timeTaking) timer() {
	defer func() {
		tt.ticker.Stop()
		Debugf("timer stopped!\n")
	}()

	for !tt.done {
		select {
		case fn := <-tt.fnCh:
			fn(tt)

		case now := <-tt.ticker.C:

			if !tt.pause.IsZero() {
				diff := now.Sub(tt.pause)
				tt.ctrl.app.QueueUpdateDraw(func() {
					tt.ctrl.showPause(diff)
				})
				continue
			}

			diff := now.Sub(tt.start)
			runTime := diff - tt.totalPauses

			var save bool
			if mins := int(diff.Minutes()); tt.lastSave != mins {
				save = true
				tt.lastSave = mins
			}

			start := tt.start
			tt.ctrl.app.QueueUpdateDraw(func() {
				if save {
					if err := tt.ctrl.saveRecovery(start, start.Add(runTime)); err != nil {
						Errorf("Error storing recovery: %v\n", err)
					}
				}
				tt.ctrl.showRunning(runTime)
			})
		}
	}
}

func (tt *timeTaking) doQuit(when time.Time) func() {
	after := make(chan func())
	tt.fnCh <- func(tt *timeTaking) {
		tt.done = true
		var pause time.Duration
		if !tt.pause.IsZero() {
			pause = when.Sub(tt.pause)
		}
		start, end := tt.start, when.Add(-tt.totalPauses).Add(-pause)
		after <- func() { tt.stopTaking(start, end) }
	}
	return <-after
}

func (tt *timeTaking) stopTaking(start, end time.Time) {
	c := tt.ctrl

	c.state = IgnoreState

	c.view.footer.ask(
		c.config.theme.Question,
		Translate("enter.description")+" ", "")

	c.app.SetFocus(c.view.footer.question)

	c.view.footer.question.SetAutocompleteFunc(func(text string) []string {
		if text == "" {
			return nil
		}
		text = strings.ToLower(text)

		var result []string
		dupes := make(map[string]bool)

		const maxEntries = 25

		for _, e := range tt.ctrl.project.Entries {
			if strings.Contains(strings.ToLower(e.Desc), text) && !dupes[e.Desc] {
				dupes[e.Desc] = true
				if result = append(result, e.Desc); len(result) >= maxEntries {
					break
				}
			}
		}
		return result
	})

	c.view.footer.question.SetDoneFunc(func(key tcell.Key) {
		// c.view.footer.question.SetAutocompleteFunc(nil)
		c.view.footer.question.SetDoneFunc(nil)

		var description string
		if key == tcell.KeyEnter {
			description = c.view.footer.question.GetText()
		}
		c.view.footer.question.SetText("")

		if err := c.stopProject(start, end, description); err != nil {
			Errorf("stop running failed: %v\n", err)
		}

		c.view.projects.updateProjectTime(c.project)

		if err := c.view.entries.fillEntries(); err != nil {
			Errorf("displaying entries failed: %v\n", err)
		}

		c.view.projects.allProjectsTime()

		c.view.footer.info(
			c.config.theme.Info,
			Translate("choose.project"))

		c.app.SetFocus(c.view.projects.table)
		c.state = ProjectState
	})
}

func (tt *timeTaking) doUnpause(when time.Time) {
	pause := when.Sub(tt.pause)
	tt.pause = time.Time{}
	tt.totalPauses += pause
}

func (tt *timeTaking) addTime(duration time.Duration) {
	// First try to reduce the old pauses.
	if tt.totalPauses > 0 {
		if tt.totalPauses >= duration {
			tt.totalPauses -= duration
			duration = 0
		} else {
			tt.totalPauses = 0
			duration -= tt.totalPauses
		}
	}

	now := time.Now()
	var pause time.Duration
	if !tt.pause.IsZero() {
		pause = now.Sub(tt.pause)
	}

	// If we still have a rest try to reduce current pause.
	if duration > 0 && pause > time.Second {
		// Always leave a second to stay in pause mode.
		if d := pause - time.Second; duration >= d {
			duration -= d
			tt.pause = tt.pause.Add(-d)
			pause = time.Second
		} else { // duration < d
			d -= duration
			tt.pause = tt.pause.Add(d)
			pause -= duration
			duration = 0
		}
	}

	// If we have still a rest move start point back in time.
	if duration > 0 {
		tt.start = tt.start.Add(-duration)
	}

	start, runTime := tt.start, now.Sub(tt.start)-tt.totalPauses-pause
	tt.ctrl.app.QueueUpdateDraw(func() {
		if err := tt.ctrl.saveRecovery(start, start.Add(runTime)); err != nil {
			Errorf("error saving recovery: %v\n", err)
		}
		tt.ctrl.showRunning(runTime)
	})
}

func (tt *timeTaking) subTime(duration time.Duration) {
	now := time.Now()

	var pause time.Duration
	if !tt.pause.IsZero() {
		pause = now.Sub(tt.pause)
	}

	total := now.Sub(tt.start) - tt.totalPauses - pause

	if duration >= total {
		duration = total
	}

	tt.totalPauses += duration

	start, runTime := tt.start, now.Sub(tt.start)-tt.totalPauses-pause
	tt.ctrl.app.QueueUpdateDraw(func() {
		if err := tt.ctrl.saveRecovery(start, start.Add(runTime)); err != nil {
			Errorf("error saving recovery: %v\n", err)
		}
		tt.ctrl.showRunning(runTime)
	})
}

func (tt *timeTaking) running(ctrl *Controller, event *tcell.EventKey) (State, error) {
	Debugf("running state\n")
	switch keys := &ctrl.config.Keys; {
	case keys.ProjectPause.is(event):
		// Switch to Pause mode
		when := event.When()
		tt.fnCh <- func(tt *timeTaking) { tt.pause = when }
		tt.ctrl.showPause(0)
		return tt.pausing, nil

	case keys.Enter.is(event):
		// Quit time taking.
		when := event.When()
		tt.doQuit(when)()
		return IgnoreState, nil

	case keys.AddTime.is(event):
		tt.handleMinutesInput((*timeTaking).addTime)
		showAddTime(ctrl, Translate("enter.time.add"))
		return IgnoreState, nil

	case keys.SubtractTime.is(event):
		tt.handleMinutesInput((*timeTaking).subTime)
		showAddTime(ctrl, Translate("enter.time.sub"))
		return IgnoreState, nil

	}
	return tt.running, nil
}

func showAddTime(c *Controller, text string) {
	c.view.projects.ask(c.config.theme.Question, text+" ", "")
	c.view.projects.question.
		SetAcceptanceFunc(acceptPositiveInteger)
}

func (tt *timeTaking) handleMinutesInput(fn func(*timeTaking, time.Duration)) {
	ctrl := tt.ctrl
	question := ctrl.view.projects.question

	question.SetDoneFunc(func(key tcell.Key) {
		question.SetDoneFunc(nil)
		question.SetAcceptanceFunc(nil)
		text := question.GetText()
		question.SetText("")
		if key == tcell.KeyEnter {
			if v, err := strconv.Atoi(text); err == nil {
				tt.fnCh <- func(tt *timeTaking) {
					fn(tt, time.Duration(v)*time.Minute)
				}
			}
		}
		ctrl.view.projects.allProjectsTime()
		ctrl.app.SetFocus(ctrl.view.projects.table)
		ctrl.state = tt.running
	})
}

func (tt *timeTaking) pausing(ctrl *Controller, event *tcell.EventKey) (State, error) {
	Debugf("pausing state\n")
	switch keys := &ctrl.config.Keys; {
	case keys.ProjectPause.is(event):
		// Switch to Pause mode
		when := event.When()
		tt.fnCh <- func(tt *timeTaking) {
			tt.doUnpause(when)
		}
		ctrl.view.projects.allProjectsTime()
		return tt.running, nil

	case keys.Enter.is(event):
		// Quit time taking.
		when := event.When()
		tt.doQuit(when)()
		return IgnoreState, nil
	}
	return tt.pausing, nil
}
