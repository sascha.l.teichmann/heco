// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"log"

	"golang.org/x/text/language"
	"golang.org/x/text/language/display"

	locale "github.com/Xuanwo/go-locale"
)

type LangLookup map[string]string

func (ll LangLookup) Translate(msg string) string {
	if t, ok := ll[msg]; ok {
		return t
	}
	return msg
}

// TODO: Use Printer from  "golang.org/x/text/message"

var Translate = func() func(string) string {
	tag, err := locale.Detect()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	supported := []language.Tag{
		language.English, // en-US fallback
		language.German,  // de
	}

	matcher := language.NewMatcher(supported)
	tag, _, _ = matcher.Match(tag)

	if en := display.English.Tags(); en.Name(tag) == en.Name(language.German) {
		return langDE.Translate
	}
	return langEN.Translate
}()
