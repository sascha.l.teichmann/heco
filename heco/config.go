// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"errors"
	"os"
	"strings"
	"unicode/utf8"

	"github.com/gdamore/tcell/v2"
	homedir "github.com/mitchellh/go-homedir"
	ini "gopkg.in/ini.v1"
)

const (
	database    = "database"
	logging     = "logging"
	mode        = "mode"
	keybindings = "keybindings"
	theme       = "theme"
)

type RuneKey struct {
	Key  tcell.Key
	Rune rune
}

type Keys struct {
	SwitchTimeMode     RuneKey
	SwitchProjectOrder RuneKey
	SwitchLists        RuneKey
	Enter              RuneKey
	Insert             RuneKey
	Delete             RuneKey
	Escape             RuneKey
	EntryUp            RuneKey
	EntryInsert        RuneKey
	EntryDown          RuneKey
	EntryDelete        RuneKey
	EntryMove          RuneKey
	EntryEdit          RuneKey
	EntryAdjust        RuneKey
	EntryLength        RuneKey
	EntryJoin          RuneKey
	EntrySplit         RuneKey
	AddTime            RuneKey
	SubtractTime       RuneKey
	ProjectPause       RuneKey
	ProjectEdit        RuneKey
}

type ThemeEntry struct {
	Foreground tcell.Color
	Backgound  tcell.Color
}

type Theme struct {
	Header             ThemeEntry
	Footer             ThemeEntry
	EntryFooter        ThemeEntry
	ProjectFooter      ThemeEntry
	ProjectKey         ThemeEntry
	Body               ThemeEntry
	Entry              ThemeEntry
	FocusedEntry       ThemeEntry
	SelectedEntry      ThemeEntry
	SelectedFocusEntry ThemeEntry
	Info               ThemeEntry
	Question           ThemeEntry
	Running            ThemeEntry
	PausedRunning      ThemeEntry
	OpenProject        ThemeEntry
}

type Mode struct {
	Sort ProjectSortMode
	Mode ProjectMode
}

type Logging struct {
	File  string
	Level LogLevel
}

type Database struct {
	URL string
}

type Config struct {
	Database Database
	Logging  Logging
	Mode     Mode
	Keys     Keys
	theme    Theme
}

var DefaultLogging = Logging{
	File:  "heco.log",
	Level: InfoLogLevel,
}

var DefaultMode = Mode{
	Sort: idSortMode,
	Mode: weekMode,
}

var DefaultTheme = Theme{
	Header:             ThemeEntry{tcell.ColorWhite, tcell.ColorDarkBlue},
	Footer:             ThemeEntry{tcell.ColorYellow, tcell.ColorDarkBlue},
	EntryFooter:        ThemeEntry{tcell.ColorWhite, tcell.ColorDarkBlue},
	ProjectFooter:      ThemeEntry{tcell.ColorWhite, tcell.ColorDarkBlue},
	ProjectKey:         ThemeEntry{tcell.ColorWhite, tcell.ColorBlue},
	Body:               ThemeEntry{tcell.ColorWhite, tcell.ColorBlack},
	Entry:              ThemeEntry{tcell.ColorWhite, tcell.ColorDarkBlue},
	FocusedEntry:       ThemeEntry{tcell.ColorWhite, tcell.ColorDarkCyan},
	SelectedEntry:      ThemeEntry{tcell.ColorYellow, tcell.ColorLightCyan},
	SelectedFocusEntry: ThemeEntry{tcell.ColorYellow, tcell.ColorDarkCyan},
	Info:               ThemeEntry{tcell.ColorWhite, tcell.ColorDarkRed},
	Question:           ThemeEntry{tcell.ColorWhite, tcell.ColorDarkRed},
	Running:            ThemeEntry{tcell.ColorYellow, tcell.ColorDarkGreen},
	PausedRunning:      ThemeEntry{tcell.ColorWhite, tcell.ColorDarkRed},
	OpenProject:        ThemeEntry{tcell.ColorWhite, tcell.ColorLightBlue},
}

var DefaultKeys = Keys{
	SwitchTimeMode:     RuneKey{Key: tcell.KeyF1},
	SwitchProjectOrder: RuneKey{Key: tcell.KeyF2},
	SwitchLists:        RuneKey{Key: tcell.KeyTab},
	Enter:              RuneKey{Key: tcell.KeyEnter},
	Insert:             RuneKey{Key: tcell.KeyInsert},
	Delete:             RuneKey{Key: tcell.KeyDelete},
	Escape:             RuneKey{Key: tcell.KeyESC},
	EntryUp:            RuneKey{Key: tcell.KeyUp},
	EntryDown:          RuneKey{Key: tcell.KeyDown},
	EntryInsert:        RuneKey{Key: tcell.KeyRune, Rune: 'i'},
	EntryDelete:        RuneKey{Key: tcell.KeyRune, Rune: 'd'},
	EntryMove:          RuneKey{Key: tcell.KeyRune, Rune: 'm'},
	EntryEdit:          RuneKey{Key: tcell.KeyRune, Rune: 'e'},
	EntryAdjust:        RuneKey{Key: tcell.KeyRune, Rune: 'a'},
	EntryLength:        RuneKey{Key: tcell.KeyRune, Rune: 'l'},
	EntryJoin:          RuneKey{Key: tcell.KeyRune, Rune: 'j'},
	EntrySplit:         RuneKey{Key: tcell.KeyRune, Rune: 's'},
	AddTime:            RuneKey{Key: tcell.KeyRune, Rune: '+'},
	SubtractTime:       RuneKey{Key: tcell.KeyRune, Rune: '-'},
	ProjectPause:       RuneKey{Key: tcell.KeyRune, Rune: ' '},
	ProjectEdit:        RuneKey{Key: tcell.KeyDEL},
}

func (t *Theme) find(s string) *ThemeEntry {
	switch s {
	case "header":
		return &t.Header
	case "body":
		return &t.Body
	case "footer":
		return &t.Footer
	case "project_footer":
		return &t.ProjectFooter
	case "entry_footer":
		return &t.EntryFooter
	case "project_key":
		return &t.ProjectKey
	case "entry":
		return &t.Entry
	case "focus_entry":
		return &t.FocusedEntry
	case "selected_entry":
		return &t.SelectedEntry
	case "selected_focus_entry":
		return &t.SelectedFocusEntry
	case "info":
		return &t.Info
	case "question":
		return &t.Question
	case "running":
		return &t.Running
	case "paused_running":
		return &t.PausedRunning
	case "open_project":
		return &t.OpenProject
	default:
		return nil
	}
}

func (t *Theme) parse(cfg *ini.File) error {
	if cfg == nil {
		return nil
	}

	section, err := cfg.GetSection(theme)
	if err != nil {
		return nil
	}

	for _, key := range section.Keys() {
		name := strings.ToLower(key.Name())
		dst := t.find(name)
		if dst == nil {
			Warnf("theme mapping '%s' not found.\n", name)
			continue
		}
		value := strings.ToLower(key.Value())
		parts := strings.Split(value, ",")
		if len(parts) < 2 {
			Warnf("theme %s has not enough colors.\n", name)
			continue
		}
		fg, bf := removeSpace(parts[0]), removeSpace(parts[1])
		*dst = ThemeEntry{
			Foreground: tcell.GetColor(fg),
			Backgound:  tcell.GetColor(bf),
		}
	}

	return nil
}

func (rk RuneKey) is(e *tcell.EventKey) bool {
	if rk.Key == tcell.KeyRune {
		return rk.Rune == e.Rune()
	}
	return rk.Key == e.Key()
}

func (ks *Keys) find(s string) *RuneKey {
	switch s {
	case "switch_time_mode":
		return &ks.SwitchTimeMode
	case "switch_project_order":
		return &ks.SwitchProjectOrder
	case "switch_lists":
		return &ks.SwitchLists
	case "enter":
		return &ks.Enter
	case "insert":
		return &ks.Insert
	case "delete":
		return &ks.Delete
	case "escape":
		return &ks.Escape
	case "entry_up":
		return &ks.EntryUp
	case "entry_down":
		return &ks.EntryDown
	case "entry_insert":
		return &ks.EntryInsert
	case "entry_delete":
		return &ks.EntryDelete
	case "entry_move":
		return &ks.EntryMove
	case "entry_edit":
		return &ks.EntryEdit
	case "entry_adjust":
		return &ks.EntryAdjust
	case "entry_length":
		return &ks.EntryLength
	case "entry_join":
		return &ks.EntryJoin
	case "entry_split":
		return &ks.EntrySplit
	case "add_time":
		return &ks.AddTime
	case "subtract_time":
		return &ks.SubtractTime
	case "project_pause":
		return &ks.ProjectPause
	case "project_edit":
		return &ks.ProjectEdit
	default:
		return nil
	}
}

func (ks *Keys) parse(cfg *ini.File) error {
	if cfg == nil {
		return nil
	}

	section, err := cfg.GetSection(keybindings)
	if err != nil {
		return nil
	}

	// Invert map for faster lookup.
	// Maybe not worth the case because of the
	// small number of known mappings in config file.
	known := make(map[string]tcell.Key)
	for k, v := range tcell.KeyNames {
		name := strings.Map(func(r rune) rune {
			if '0' <= r && r <= '9' || 'a' <= 'r' && r <= 'z' {
				return r
			}
			return -1
		}, strings.ToLower(v))
		known[name] = k
	}

	for _, key := range section.Keys() {
		name := strings.ToLower(key.Name())
		dst := ks.find(name)
		if dst == nil {
			Warnf("key mapping '%s' not found.\n", name)
			continue
		}
		value := strings.ToLower(key.Value())
		if k, ok := known[value]; ok {
			*dst = RuneKey{Key: k}
		} else {
			if r, _ := utf8.DecodeRuneInString(value); r != utf8.RuneError {
				*dst = RuneKey{Key: tcell.KeyRune, Rune: r}
			} else {
				Warnf("cannot decode %s\n", value)
			}
		}
	}

	return nil
}

func (m *Mode) parse(cfg *ini.File) error {
	if cfg == nil {
		return nil
	}

	section, err := cfg.GetSection(mode)
	if err != nil {
		return nil
	}

	if s, _ := section.GetKey("sort"); s != nil {
		if v := parseProjectSort(s.String()); v != -1 {
			m.Sort = v
		}
	}

	if s, _ := section.GetKey("mode"); s != nil {
		if v := parseProjectMode(s.String()); v != -1 {
			m.Mode = v
		}
	}

	return nil
}

func (l *Logging) parse(cfg *ini.File) error {
	if cfg == nil {
		return nil
	}

	section, err := cfg.GetSection(logging)
	if err != nil {
		return nil
	}

	if s, _ := section.GetKey("file"); s != nil {
		file, err := homedir.Expand(s.String())
		if err != nil {
			return err
		}
		l.File = file
	}

	if s, _ := section.GetKey("level"); s != nil {
		l.Level = ParseLogLevel(s.String())
	}

	return nil
}

func (d *Database) parse(cfg *ini.File) error {
	if cfg == nil {
		return nil
	}
	section, err := cfg.GetSection(database)
	if err != nil {
		return nil
	}
	if s, _ := section.GetKey("url"); s != nil {
		d.URL = s.String()
	}
	return nil
}

var DefaultConfigFallbacks = []string{
	".hecorc",
	"~/.heco/hecorc",
	"/etc/hecorc",
}

func TryConfigs(user ...string) (*Config, error) {
	user = append(user, DefaultConfigFallbacks...)
	return LoadConfig(user...)
}

func LoadConfig(filenames ...string) (*Config, error) {

	config := Config{
		Database: Database{},
		Logging:  DefaultLogging,
		Mode:     DefaultMode,
		Keys:     DefaultKeys,
		theme:    DefaultTheme,
	}

	continueErr := errors.New("continue")

	parsers := []func(*ini.File) error{
		config.Database.parse,
		config.Logging.parse,
		config.Mode.parse,
		config.Keys.parse,
		config.theme.parse,
	}

next:
	for _, filename := range filenames {
		name, err := homedir.Expand(filename)
		if err != nil {
			return nil, err
		}
		switch err := func(name string) error {
			f, err := os.Open(name)
			if err != nil {
				return continueErr
			}
			defer f.Close()
			cfg, err := ini.Load(f)
			if err != nil {
				return err
			}
			for _, parser := range parsers {
				if err := parser(cfg); err != nil {
					return err
				}
			}
			return nil
		}(name); {
		case err == continueErr:
			continue next
		case err != nil:
			return nil, err
		}
		break
	}

	return &config, nil
}
