// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package heco

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type Controller struct {
	config  *Config
	backend Backend
	app     *tview.Application

	projects Projects
	project  *Project

	view *View

	running bool
	state   State

	modeMu     sync.RWMutex
	mode       ProjectMode
	modeFilter func(*Entry) bool
	modeTimer  *time.Timer
}

func NewController(config *Config, backend Backend) (*Controller, error) {

	c := Controller{
		config:  config,
		backend: backend,
		state:   ProjectState,
		mode:    config.Mode.Mode,
	}

	if err := c.loadActiveProjects(); err != nil {
		return nil, err
	}

	if err := c.loadRecover(); err != nil {
		return nil, err
	}

	if len(c.projects) > 0 {
		c.project = c.projects[0]
	}

	c.updateModeFilter()

	return &c, nil
}

func (c *Controller) getMode() ProjectMode {
	c.modeMu.RLock()
	defer c.modeMu.RUnlock()
	return c.mode
}

func (c *Controller) getModeFilter() func(*Entry) bool {
	c.modeMu.RLock()
	defer c.modeMu.RUnlock()
	return c.modeFilter
}

func (c *Controller) updateModeFilter() {
	// Filters are valid till tomorrow.
	now := time.Now()
	year, month, day := now.Date()
	// Add extra second to be sure we are after midnight.
	tomorrow := time.Date(year, month, day+1, 0, 0, 1, 0, time.Local)
	diff := tomorrow.Sub(now)
	c.modeMu.Lock()
	defer c.modeMu.Unlock()
	c.modeFilter = c.mode.Accept(now)
	if c.modeTimer != nil {
		c.modeTimer.Stop()
	}
	c.modeTimer = time.AfterFunc(diff, c.updateModeFilter)
}

func (c *Controller) cycleMode() {
	c.modeMu.Lock()
	defer c.modeMu.Unlock()
	c.mode = c.mode.Cycle()
	c.modeFilter = c.mode.Accept(time.Now())
}

func (c *Controller) stopModeUpdates() {
	c.modeMu.Lock()
	defer c.modeMu.Unlock()
	if c.modeTimer != nil {
		c.modeTimer.Stop()
		c.modeTimer = nil
	}
}

func (c *Controller) SetApp(app *tview.Application) {
	handler := app.GetInputCapture()
	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		// Always pass through Ctrl-C.
		if event.Key() == tcell.KeyCtrlC {
			return event
		}
		if c.state != nil {
			newState, err := c.state(c, event)
			if err != nil {
				Errorf("error: %v\n", err)
			}
			if newState != nil {
				c.state = newState
				return nil
			}
		}
		if handler != nil {
			return handler(event)
		}
		return event
	})
	c.app = app
}

func (c *Controller) quit() {
	c.stopModeUpdates()
	c.app.Stop()
}

func (c *Controller) loadActiveProjects() error {
	projects, err := c.backend.LoadProjects()
	if err != nil {
		return err
	}
	c.projects = projects
	return nil
}

func (c *Controller) loadRecover() error {
	return c.backend.LoadRecover()
}

func (c *Controller) startTimeTaking(when time.Time) *timeTaking {
	tt := &timeTaking{
		ctrl:   c,
		start:  when,
		ticker: time.NewTicker(time.Second),
		fnCh:   make(chan func(*timeTaking)),
	}
	go tt.timer()
	return tt
}

func (c *Controller) startProject(proj *Project) {
	Debugf("start project %s\n", proj.Key)
	c.project = proj
	if err := c.view.projects.switchToProject(); err != nil {
		Errorf("Switching to project failed: %v.\n", err)
		return
	}
	c.app.SetFocus(c.view.projects.table)
	if !c.running {
		tt := c.startTimeTaking(time.Now())
		c.running = true
		c.state = tt.running
		c.showRunning(0)
	}
}

func (c *Controller) stopRunning() error {
	if !c.running {
		return nil
	}
	c.running = false

	if err := c.backend.LoadRecover(); err != nil {
		Errorf("recovery failed: %v\n", err)
	}

	return nil
}

func (c *Controller) stopProject(start, stop time.Time, desc string) error {
	Debugf("stop project\n")
	if !c.running {
		Debugf("no time taking running\n")
		return nil
	}
	c.running = false

	if desc = strings.TrimSpace(desc); desc == "" {
		desc = "-no description-"
	}

	entryID, err := c.backend.insertEntry(
		c.project.ID,
		start, stop,
		desc)
	if err != nil {
		return err
	}
	Debugf("entry id: %d\n", entryID)
	entry := NewEntry(entryID, start, stop, desc)
	c.project.AddEntry(entry)
	return nil
}

func (c *Controller) saveRecovery(start, stop time.Time) error {
	Debugf("Save recovery\n")
	if !c.running {
		return nil
	}
	return c.backend.insertRecover(
		c.project.ID, start, stop, "-no description-")
}

func (c *Controller) Shutdown() error {
	Debugf("shutdown controller\n")
	return c.stopRunning()
}

func (c *Controller) showPause(pause time.Duration) {

	msg := fmt.Sprintf(
		Translate("break"),
		HumanTime(pause),
		fmt.Sprintf(
			Translate("space.continue"),
			c.config.Keys.ProjectPause.Rune))

	c.view.projects.info(
		c.config.theme.PausedRunning, msg)
}

func (c *Controller) showRunning(d time.Duration) {
	if c.project == nil {
		return
	}

	msg := fmt.Sprintf(
		Translate("running.on"), HumanTime(d), c.project.Desc)

	c.view.footer.info(c.config.theme.Running, msg)
}
