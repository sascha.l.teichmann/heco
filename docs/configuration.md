# Configuration

Heco can be configured with a configuration INI file.
Without given a specific path with the `-c` flag at startup
if will look at `~/.heco/hecorc` .
There are five optional sections `[database]`, `[logging]`, `[mode]`, `[keys]` and `[theme]`.
**database** determine the used database.
**logging** sets up the logging of the application.
**mode** determines the view mode. **keys** will
alter your keyboard mapping and **theme** the optics.

`[database]`:

|Keyword|Default value|Meaning|
|-------|-------------|-------|
|url    |             |URL to database|

If the `url` value is set it is interpreted as the filename of the database to use.
If the value starts with `postgresql:` it is interpreted as a connection string
to a PostgreSQL database. E.g. `postgresql://user:password@localhost/db` would
try to connect to a database `db` with `user`/`password` at `localhost`. 
**Attention**: The PostgreSQL database needs to be setup before using heco.
See [contrib/postgresql.sql](../contrib/postgresql.sql) for an init script example.

`[logging]`:

|Keyword|Default value|Meaning|
|-------|-------------|-------|
|file   |heco.log     |File path to the log file|
|level  |info         |Set log level to **trace**, **debug**, **info**, **warn**, **error** or **fatal**|

`[mode]`:

|Keyword|Default value|Meaning|
|-------|-------------|-------|
|mode   |week         | Display **day**, **week**, **last_week**, **month**, **year** or **total** |
|sort   |id           | Order projects by **id**, **key**, **desc**-ing or as **tree** |

`[keys]`:

|Keyword|Default value|Meaning|
|-------|-------------|-------|
|switch_time_mode|F1|Cylcle through sum modes|
|switch_project_order|F2|Cycle through different orderings of projects|
|switch_lists|Tab|Switch between entries and project window|
|insert|Insert|Create a new project (project window)|
|enter|Enter| Start time taking|
|add_time|+|Add time to current time taking|
|substract_time|-|Subtract time from current time taking|
|project_pause|space|Pause/unpause running time taking|
|project_edit|Backspace|Edit a project (project window)|
|entry_move|m|Moves one or more entries (entries window)|
|entry_delete|d|Deletes one or more entries (entries window)|
|entry_insert|i|Add an entry without time-taking (entries window)|
|entry_edit|e|Edit description of entry (entries window)|
|entry_adjust|a|Alter start time of entry (entries window)|
|entry_length|l|Alter duration of entry (entries window)|
|entry_join|j|Join entries (entries window)|
|entry_split|s|Split entry (entries window)|

`[theme]`: Some aspects of the coloring of heco can be changed
by configuration. Due to the limits of terminals and the
[library](https://github.com/rivo/tview) heco uses for rendering your mileage may vary.
An item is a pair of comma separated foreground and background colors.

|Keyword|Default values|Meaning|
|-------|--------------|-------|
|header|"white, dark blue"|The header line|
|footer|"yellow, dark blue"|The footer line|
|body|"yellow, dark blue"|The background|
|entry_footer|"white, dark blue"|Footer of the entry window|
|project_footer|"white, dark blue"|Footer of the project window|
|project_key|"white, blue"|The column of the shortcuts in the project window|
|entry|"white, dark blue"|project and entry in the entries window|
|selected_entry|"white, dark cyan"|A selected entry|
|info|"white, dark red"|Information text|
|question|"white, dark red"|Questions text|
|running|"yellow, dark green"|Running time taking|
|paused_running|"white, dark red"|Pausing time taking|

