// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/rivo/tview"

	"gitlab.com/sascha.l.teichmann/heco/heco"
)

const usageTxt = `usage: %s [option]... [database]
A simple time taking tool for the command line.
Version: %s
Configuration fallback files: %s
Options:
`

func usage() {
	out := flag.CommandLine.Output()
	fallbacks := strings.Join(heco.DefaultConfigFallbacks, ", ")
	fmt.Fprintf(out, usageTxt, os.Args[0], heco.Version, fallbacks)
	flag.PrintDefaults()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func run(cfg *heco.Config, filename, logFile, logLevel string, initOnly bool) error {
	ctx := context.Background()

	if logFile == "" {
		cfg.Logging.File = logFile
	}
	if err := heco.SetupLog(cfg.Logging.File, 0600); err != nil {
		return err
	}
	defer heco.ShutdownLog()

	if logLevel != "" {
		cfg.Logging.Level = heco.ParseLogLevel(logLevel)
	}
	heco.SetLogLevel(cfg.Logging.Level)

	backend, err := heco.NewBackend(ctx, filename)
	if err != nil {
		return err
	}
	defer backend.Close()

	if initOnly {
		return nil
	}

	ctrl, err := heco.NewController(cfg, backend)
	if err != nil {
		return err
	}
	defer ctrl.Shutdown()

	app := tview.NewApplication()
	ctrl.SetApp(app)

	if err := heco.BuildView(app, ctrl); err != nil {
		return err
	}

	return app.Run()
}

func main() {

	var (
		logFile  string
		logLevel string
		cfgFile  string
		initOnly bool
	)

	flag.StringVar(
		&logFile,
		"logfile",
		"heco.log",
		"write log information to FILE")
	flag.StringVar(
		&logFile,
		"l",
		"heco.log",
		"write log information to FILE (shorthand)")
	flag.BoolVar(
		&initOnly,
		"init-only",
		false,
		"create databasefile if necessary and exit")
	flag.BoolVar(
		&initOnly,
		"i",
		false,
		"create databasefile if necessary and exit (shortcut)")
	flag.StringVar(
		&logLevel,
		"loglevel",
		"",
		"set log level to trace, debug, info, warn, error or fatal")
	flag.StringVar(
		&logLevel,
		"d",
		"",
		"set log level to trace, debug, info, warn, error or fatal (shortcut)")
	flag.StringVar(
		&cfgFile,
		"c",
		"",
		"Load configuration from FILE (shorthand)")
	flag.StringVar(
		&cfgFile,
		"config",
		"",
		"Load configuration from FILE")

	flag.Usage = usage

	flag.Parse()

	var cfg *heco.Config
	var err error

	if cfgFile != "" {
		cfg, err = heco.TryConfigs(cfgFile)
	} else {
		cfg, err = heco.TryConfigs()
	}
	check(err)

	var filename string
	expand := true
	switch {
	case flag.NArg() > 0:
		filename = flag.Arg(0)
	case cfg.Database.URL != "":
		filename = cfg.Database.URL
		expand = false
	default:
		filename = "~/.heco/" + heco.DefaultDatabase
	}

	if expand {
		filename, err = homedir.Expand(filename)
		check(err)
	}

	check(run(cfg, filename, logFile, logLevel, initOnly))
}
