// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package main

import (
	"context"
	"database/sql"
	"math"
	"text/template"
	"time"
)

type (
	meta struct {
		names []string
	}

	row struct {
		meta   *meta
		Values []interface{}
	}

	rows []*row

	group struct {
		Key  interface{}
		Rows rows
	}

	sqlSupport struct {
		db *sql.DB
	}

	number struct {
		i int64
		f float64
	}
)

func (r *row) find(name string) int {
	for i, n := range r.meta.names {
		if n == name {
			return i
		}
	}
	return -1
}

func (r *row) Get(name string) interface{} {
	if i := r.find(name); i >= 0 {
		return r.Values[i]
	}
	return nil
}

func (r *row) Value(name string) interface{} {
	return r.Index(r.find(name))
}

func (r *row) Index(idx int) interface{} {
	if idx < 0 || idx >= len(r.Values) {
		return nil
	}
	return r.Values[idx]
}

func (rs rows) GroupBy(key string) []group {
	if len(rs) == 0 {
		return nil
	}
	idx := rs[0].find(key)
	if idx < 0 {
		return []group{{Key: nil, Rows: rs}}
	}
	var gs []group
	indices := make(map[interface{}]int)
	for _, r := range rs {
		k := r.Index(idx)
		idx, ok := indices[k]
		if !ok {
			idx = len(gs)
			indices[k] = idx
			gs = append(gs, group{Key: k})
		}
		gs[idx].Rows = append(gs[idx].Rows, r)
	}
	return gs
}

func (rs rows) Sum(name string) number {
	if len(rs) == 0 {
		return number{}
	}
	idx := rs[0].find(name)
	if idx < 0 {
		return number{}
	}
	var sum number
	for _, r := range rs {
		sum = sum.Add(r.Values[idx])
	}
	return sum
}

func checkSQL(err error) {
	if err != nil {
		panic(&template.ExecError{Err: err})
	}
}

func (ss *sqlSupport) SQL(sql string, args ...interface{}) rows {
	ctx := context.Background()
	con, err := ss.db.Conn(ctx)
	checkSQL(err)
	defer con.Close()
	rs, err := con.QueryContext(ctx, sql, args...)
	checkSQL(err)
	defer rs.Close()

	columns, err := rs.Columns()
	checkSQL(err)

	m := &meta{names: columns}

	var result rows

	ptrs := make([]interface{}, len(columns))

	for rs.Next() {
		values := make([]interface{}, len(columns))
		for i := range values {
			ptrs[i] = &values[i]
		}
		checkSQL(rs.Scan(ptrs...))
		result = append(result, &row{meta: m, Values: values})
	}

	checkSQL(rs.Err())

	return result
}

func NewNumber() number { return number{} }

func (n number) Add(a ...interface{}) number {
	out := number{n.i, n.f}
	for _, x := range a {
		switch v := x.(type) {
		case int:
			out.i += int64(v)
		case int64:
			out.i += v
		case float64:
			out.f += v
		case number:
			out.i += v.i
			out.f += v.f
		}
	}
	return out
}

func (n number) Int() int64     { return n.i + int64(n.f) }
func (n number) Float() float64 { return float64(n.i) + n.f }

func Seconds(x interface{}) time.Duration {
	var t time.Duration
	switch v := x.(type) {
	case int:
		t = time.Duration(int64(v))
	case int64:
		t = time.Duration(v)
	case float64:
		t = time.Duration(math.Round(v))
	case number:
		t = time.Duration(v.Int())
	}
	return t * time.Second
}

func isoweek(s string, year, week int64) (bool, error) {
	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return false, err
	}
	y, w := t.ISOWeek()
	return int64(y) == year && int64(w) == week, nil
}
