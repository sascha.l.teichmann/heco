// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

//go:generate go run gen.go -d templates -o embedded_templates.go

package main

import (
	"os"

	"github.com/mitchellh/go-homedir"
)

type namedTemplate struct {
	name string
	text string
}

func findTemplate(name string) (*namedTemplate, error) {
	// Handling default
	if name == "" && len(templateNames) > 0 {
		return &namedTemplate{
			name: templateNames[0],
			text: string(embeddedTemplates[0]),
		}, nil
	}
	for i, n := range templateNames {
		if n == name {
			return &namedTemplate{
				name: name,
				text: string(embeddedTemplates[i]),
			}, nil
		}
	}
	path, err := homedir.Expand("~/.heco/templates/" + name)
	if err != nil {
		return nil, err
	}
	bytes, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return &namedTemplate{name: name, text: string(bytes)}, nil
}
