// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package main

import (
	"bufio"
	"database/sql"
	"flag"
	"log"
	"os"
	"text/template"
	"time"

	sqlite "github.com/mattn/go-sqlite3"

	"github.com/mitchellh/go-homedir"
	"gitlab.com/sascha.l.teichmann/heco/heco"
)

func main() {
	sql.Register("sqlite3_isoweek", &sqlite.SQLiteDriver{
		ConnectHook: func(conn *sqlite.SQLiteConn) error {
			return conn.RegisterFunc("isoweek", isoweek, true)
		},
	})
	var (
		dbFile   string
		tmplName string
		user     string
		pattern  string
		lastWeek bool
		week     int
		year     int
		empty    bool
	)

	defaultUser := os.Getenv("USER")

	flag.StringVar(
		&dbFile,
		"database",
		"~/.heco/"+heco.DefaultDatabase,
		"The database to extract data from.")
	flag.StringVar(
		&dbFile,
		"d",
		"~/.heco/"+heco.DefaultDatabase,
		"The database to extract data from (shorthand).")
	flag.StringVar(
		&tmplName,
		"template",
		"weekly",
		"The name of the template.")
	flag.StringVar(
		&tmplName,
		"t",
		"weekly",
		"The name of the template (shorthand).")
	flag.BoolVar(
		&lastWeek,
		"lastweek",
		false,
		"Entries of the last working week.")
	flag.BoolVar(
		&lastWeek,
		"c",
		false,
		"Entries of the last working week (shorthand).")
	flag.BoolVar(
		&empty,
		"empty",
		false,
		"Show projects whithout an entry.")
	flag.BoolVar(
		&empty,
		"m",
		false,
		"Show projects whithout an entry (shorthand).")
	flag.IntVar(
		&year,
		"year",
		-1,
		"Year to use (negative value: current).")
	flag.IntVar(
		&year,
		"y",
		-1,
		"Year to use (negative value: current) (shorthand).")
	flag.IntVar(
		&week,
		"week",
		-1,
		"Week to use (negative value: current)")
	flag.IntVar(
		&week,
		"w",
		-1,
		"Week to use (negative value: current) (shorthand)")
	flag.StringVar(
		&user,
		"user",
		defaultUser,
		"User for reports.")
	flag.StringVar(
		&user,
		"u",
		defaultUser,
		"User for reports (shorthand).")
	flag.StringVar(
		&pattern,
		"project",
		"%",
		"Key (SQL pattern) of output projects.")
	flag.StringVar(
		&pattern,
		"p",
		"%",
		"Key (SQL pattern) of output projects (shorthand).")

	flag.Parse()

	if lastWeek {
		last := time.Now().AddDate(0, 0, -7)
		year, week = last.ISOWeek()
	}

	if week != -1 && year == -1 {
		year = time.Now().Year()
	}

	ntmpl, err := findTemplate(tmplName)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if ntmpl == nil {
		log.Fatalf("No template '%s' found.\n", tmplName)
	}

	if dbFile, err = homedir.Expand(dbFile); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if err := func() error {
		db, err := sql.Open("sqlite3_isoweek", dbFile)
		if err != nil {
			return err
		}
		defer db.Close()

		ss := sqlSupport{db: db}

		funcs := template.FuncMap{
			"SQL":        ss.SQL,
			"ShortTime":  heco.ShortTime,
			"DateFormat": heco.DateFormat,
			"NewNumber":  NewNumber,
			"Seconds":    Seconds,
			"Runes":      func(s string) []rune { return []rune(s) },
		}

		tmpl, err := template.New(ntmpl.name).Funcs(funcs).Parse(ntmpl.text)
		if err != nil {
			log.Fatalf("err: %v\n", err)
		}

		context := map[string]interface{}{
			"key":   pattern,
			"user":  user,
			"week":  int64(week),
			"year":  int64(year),
			"empty": empty,
		}

		out := bufio.NewWriter(os.Stdout)

		if err := tmpl.Execute(out, context); err != nil {
			return err
		}

		return out.Flush()
	}(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
