module gitlab.com/sascha.l.teichmann/heco

go 1.23

require (
	github.com/Xuanwo/go-locale v1.1.3
	github.com/gdamore/tcell/v2 v2.8.1
	github.com/jackc/pgx/v5 v5.7.2
	github.com/mattn/go-sqlite3 v1.14.24
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rivo/tview v0.0.0-20241227133733-17b7edb88c57
	golang.org/x/text v0.22.0
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/gdamore/encoding v1.0.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/puddle/v2 v2.2.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
	golang.org/x/crypto v0.33.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/term v0.29.0 // indirect
)
